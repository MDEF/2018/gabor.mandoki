---
layout: page
title :
permalink: /aboutme/
---

&nbsp;

![]({{site.baseurl}}/assets/GaborMandoki.jpg)

I am Gabor, a communication designer from Munich, Germany, however with Hungarian roots.  

Before I decided to study design, I was a semi-professional musician and composed music for different kind of projects and companies.
I considered studying music, but that wasn’t enough for me. I wanted to study something that combines know-how and a creative way of finding solutions for any given challenge and design seemed to be the perfect combination. During my studies I expended my knowledge a lot in the areas of art direction, graphic design, interface and multimedia design as well as editorial.
After graduating at the IFOG academy, I worked on different projects. My tasks and challenges varied. I took part in creating a book, producing a documentary movie and organizing a multi-artist concert series.
Besides these experiences in professional areas, I always had a high interest in technological
developments. I am always eager to hear about new acquisitions in fields of mobility, society, infrastructure, living and communication.

In the future, I see me being part of a project or a smaller company, with the intention to enrich the community or society with a product or service. I don’t know exactly what this will look like. I hope that the next year will help me with that. I would like to focus on topics like politics, education, socio-economics, medical care and (new) technologies.

I could imagine living in a city or near by one. I could also imagine having different projects in different fields, not focusing specifically on one.
