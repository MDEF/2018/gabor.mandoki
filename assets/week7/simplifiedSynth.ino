const int speakerPin = 11;
const int buttonPin1 = 2;
const int buttonPin2 = 3;
const int buttonPin3 = 4;
const int buttonPin4 = 5;
const int buttonPin5 = 6;

int buttonState1 = 0; 
int buttonState2 = 0;
int buttonState3 = 0;
int buttonState4 = 0;
int buttonState5 = 0;


void setup() {
  
  pinMode(speakerPin, OUTPUT);
  pinMode(buttonPin1, INPUT);
  pinMode(buttonPin2, INPUT);
  pinMode(buttonPin3, INPUT);
  pinMode(buttonPin4, INPUT);
  pinMode(buttonPin5, INPUT);
}


void loop() {

buttonState1 = digitalRead(buttonPin1);
buttonState2 = digitalRead(buttonPin2);
buttonState3 = digitalRead(buttonPin3);
buttonState4 = digitalRead(buttonPin4);
buttonState5 = digitalRead(buttonPin5);
  
//C4
  if (buttonState1 == HIGH) {
        digitalWrite(speakerPin, HIGH);  
        delayMicroseconds (1000);  
        digitalWrite(speakerPin, LOW);    
        delayMicroseconds (1000);
  } else {    
    digitalWrite(speakerPin, LOW);  
  }

//D4
  if (buttonState2 == HIGH) {
        digitalWrite(speakerPin, HIGH);  
        delayMicroseconds (1703);  
        digitalWrite(speakerPin, LOW);    
        delayMicroseconds (1703);
  } else {    
    digitalWrite(speakerPin, LOW);    
  }

//E4
  if (buttonState3 == HIGH) {
        digitalWrite(speakerPin, HIGH);  
        delayMicroseconds (1517);  
        digitalWrite(speakerPin, LOW);    
        delayMicroseconds (1517);
  } else {    
    digitalWrite(speakerPin, LOW);    
  }

//F4
  if (buttonState4 == HIGH) {
        digitalWrite(speakerPin, HIGH);  
        delayMicroseconds (1432);  
        digitalWrite(speakerPin, LOW);    
        delayMicroseconds (1432);
  } else {    
    digitalWrite(speakerPin, LOW);    
  }

//G4
  if (buttonState5 == HIGH) {
        digitalWrite(speakerPin, HIGH);  
        delayMicroseconds (1276);  
        digitalWrite(speakerPin, LOW);    
        delayMicroseconds (1276);
  } else {    
    digitalWrite(speakerPin, LOW);    
  }
  
}
