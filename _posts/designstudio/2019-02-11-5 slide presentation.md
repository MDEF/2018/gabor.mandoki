---
layout: post
title:  "Design Studio: 5 slide presentation."
date:   2019-02-11 13:00:00 +0530
categories: designstudio term2


---

![]({{site.baseurl}}/assets/term2/designstudios/presentation_5slide/1slide.jpeg)

![]({{site.baseurl}}/assets/term2/designstudios/presentation_5slide/2slide.jpeg)

![]({{site.baseurl}}/assets/term2/designstudios/presentation_5slide/3slide.jpeg)

![]({{site.baseurl}}/assets/term2/designstudios/presentation_5slide/4slide.jpeg)

![]({{site.baseurl}}/assets/term2/designstudios/presentation_5slide/5slide.jpeg)
