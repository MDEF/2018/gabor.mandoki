---
layout: post
title:  "Design Studio: Shenzhen."
date:   2019-01-28 15:00:00 +0530
categories: designstudio term2



---

![]({{site.baseurl}}/assets/term2/shenzhen/IMG_3997.jpg)

# Shenzhen.

------

&nbsp;

The first two weeks of our second trimester, the MDEF crew went to Shenzhen, China to experience the future on the other side of the world. We were visiting factories, companies and meeting interesting people from the fields of manufacturing, design and innovation. And of course, being in China alone led to cultural exchange and understanding.

We were divided into groups of interest. I and my group focused on AI and cognitive systems.

&nbsp;

#### the video.

<iframe src="https://player.vimeo.com/video/312961246" width="900" height="600" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

From our overserving an analytical position we divided our results into three main topics:

&nbsp;

## Infrastructure.

The first point we were focusing on was the infrastructure. In Shenzhen, it is perfectly shown how fast China develops and how China is capable to apply or adopt new things and structures in society at a really high pace.

The payment infrastructure, WeChat Pay or AliPay, the super fast railway, which is one of the best railway networks worldwide or the only electric cabs, to name a few, are perfect examples for that.

So how will this attitude of making and creating which has made Shenzhen the city it is today be reflected in the
development of AI?

Should this attitude be applied to the development of AI or is a more reflective approach needed?

Do we need global ethical standards in the development of AI? Ethical standards not in the sense how an AI behaves but in what framework AIs should be developed. Do we need an international agreement for AIs with a superintelligence approach to be sandboxed for example?

The interesting thing is the perspective of how Min Wanli, Vice-president of Alibaba Cloud and Chief
Data Scientist of Alibaba evaluated this philosophy.

He said, that compared to the AI systems US companies are developing, China is some levels behind in terms of capability and power. But China is already applying their technologies to improve infrastructure and social living. While US companies demonstrate their capabilities by batteling gamers on platforms like AlphaGo, China already planned a city completely designed by an AI system, reduced the time of the ambulance arriving to its destination by 20% with the help of AI and recognizes trash on the streets with the help of their CCTV network and AI recognition systems to send the street cleaners to certain points rather then let them drive the same route every day.

![]({{site.baseurl}}/assets/term2/shenzhen/IMG_4110.jpg)

&nbsp;

## Data & Trust.

The second aspect we were looking at was data and the trust society has in companies dealing with their data.

What is the role of companies in contrast to the government?

Does mass data collection imply control of some degree?

Does the willingness to give away data sacrifice free will & trust?

We tried to understand how the common sense of data and privacy differs from the European and US understanding.
We hadn't had a conversation about that topic with someone from China, only one comment about the fear of the illegal data trade. Unfortunately, I couldn't find anything about it yet but I will keep on my research list.

The things we could observe, people were absolutely fine with sharing a huge amount of personal data. Not only with services like WeChat or Baidu, as these companies have, let us call it an open policy regarding data, but also using smartwatches for kids to track their GPS data or having cameras observing babies and share this data as well.

At this point I have to say, yes especially in Europe we have a very strict data policy, but still, most people in society are sharing their data to services the same way China does. Is it so different sharing data with Tencent, Baidu and Alibaba compared to Facebook, Google or Amazon? I think in the end it makes no difference. We in the western hemisphere feel super superior with our privacy and data policy but I think the only difference is that Chinese companies are open about collecting data and using it for developments like AI systems or observation and the western companies pretend to be safe while listening and reading every conversation we have. And we also agreed to that.

So in the end, my assessment of the global data and privacy situation is, that most people are ready to share data with companies to receive coast free service. Maybe the only difference to China is that data collection is governmentally certified while in Europe the governments still pretend to protect us from data collectors.

![]({{site.baseurl}}/assets/term2/shenzhen/IMG_4049.jpg)

&nbsp;

## Innovation.

Because AI is obviously the next big thing, we focused also on how innovation is defined in China compared to our understanding. Is the meaning of innovation inventing things from scratch or improve exiting?
Or is it implement something to society?

During our first visit, we heard about electric cars produced right now in China. The system was taking an existing low range low power electric engine and put it into a lightweight car and turn it into mass production. And it was called innovation.

On the other hand, China also focusses more on their needs. Our western understanding of innovation often ends with a demonstration of power like the AlphaGO AI. But what can we use it for? While Alibaba uses his less powerful Algorithms to reduce the time of the ambulance arriving at the accident or the detect trash on the street to reform the street-cleaner schedule.

In my opinion, Chines inventions are often improvements or even only applications of inventions. But the difference is they don’t just talk. They take something and do. Transform existing to fit the Chinese market which is a huge one.

Another aspect of innovation is the personification of AI. You see a lot of robots and droids. We also saw Robots especially for kids during our trip to China. So you can say it is the physical personification of AI. What will the effect in society be if this goes on? Will we have a Star Wars situation with an easy coexistence with very specialized droids? Will there be a situation like in the movie HER?

As a consequence… how will kids relate to AIs? Will they have AI friends? As I said, we saw educational bots for kids, smartwatches only for kids. So the concerns we have implementing new technologies in our everyday life, especially to our kids are not the same. We would have loved to ask Chinese parents, for example, what they think about that? More use than danger?

And in the end, what is the role of AI in the next decade? Right now they are working hard to improve industry and society. Planning cities, applying social credit systems etc. But where are we heading? Where is China heading? And finally, what are the worries of the Chinese society regarding this deep implementation of AI systems into their lives?

![]({{site.baseurl}}/assets/term2/shenzhen/yiran-ding-402896-unsplash.jpg)

&nbsp;

## my conclusion.

The trip to Shenzhen was very good to get an idea of what is happening right now in China. How they develop, how the infrastructure and society works. Of course, we have only been there for a short period and don't speak Chinese what makes it difficult to get a deeper insight. What I missed on this trip was a discussion with Chinese citizens about certain topics like trust, AI development, fears or perspectives. The visit throws up a lot of questions. I would love to have them discussed with Chinese experts or students. Maybe there will be another chance to be in contact. I am, but I already did before my time in Barcelone, considering living for a while in Shanghai or Hong Kong. I love the differences and the similarities and I could absolutely imagine to build up a life deeply connected to China.

------

&nbsp;

#### posts from my fellow students.

&nbsp;



[Maite Villar.](https://mdef.gitlab.io/maite.villar/reflections/Shenzhen/)

[Silvia Ferrari.](https://mdef.gitlab.io/silvia.ferrari/envisioning/on-china/)

[Oliver Juggins.](https://mdef.gitlab.io/oliver.juggins/masterproject/china/)
