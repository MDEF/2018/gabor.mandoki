---
layout: post
title:  "Atlas of the Weak Signals."
date:   2019-04-19 10:00:00 +0530
categories: atlasoftheweaksignals term2


---

<iframe
  src="https://embed.kumu.io/3ad6defa64abaab4ad124774dedf5442"
  width="740" height="450" frameborder="0"></iframe>



# Atlas of the Weak Signals.

------

This course in the second term gave us tools to define and explore weak signals - which are defined as "snippets you almost can't detect because of the background noise." 

The course was divided into two main aspects. One was to work and talk about weak signals and to create speculative future scenarios based on them. The other course was to find and cluster signals related to the main topics.

the topics related to my master project:

### Surveillance Capitalism

- Manipulation
- Circular Data Economy
- Attention Protection

### The Anthropocene

- Longtermism

### Future of Jobs

- Human Machine Creative Colabo
- Tech for Equality

&nbsp;

## Weak Signals and my master project. 

The first week of the weak signal courses collided with my decision to switch the focus of my master project from artificial intelligence and assistance systems to attention sovereignty. The lecture about Surveillance Capitalism was the perfect fuel to kickstart my research. I rushed through most of the articles my classmates posted in our database to get an overview as I was way behind by switching my topic so late. 

On the one hand, the lectures, especially the first week, have been perfect resources for me with the combined help of my fellow students. On the other hand, by discussing such a variety of topics with an equal variety of backgrounds and perspectives widens your field of perspective. I also discovered, that in that way we not only gathered weak signals from sources like the internet but also discovered weak signals among us. 

So, the topics manipulation, circular data economy and attention protection build the base for my research. The analysis is how we get hooked to services and devices answers the question of why I am doing my project. All methods discussed by attention protection, built the core element on how I want to tackle this question. The week of future of jobs, I haven't focused on a topic really related to my master project, but it confronted me with the question how we shift the use of the technology that is known to focus on our attention, to a more collective and distributive way.

If you read the first page of my [final presentation of the second term]({{site.baseurl}}/masterproject/), you will see how the weak signals are directly related to my thesis. 

My conclusion how the weak signals courses influenced my master project is, that the research methods we learned are one of my core elements now. Additionally, these methods helped me to find the weak signal I am tackling in my main topic very quick which is attention sovereignty and the influence on the social interaction. 

&nbsp;

## Weak Signals and my future projects.

Besides the topics I am already using for my master project, the human-machine collabo and the political topics influenced me the most for my future. 

The human-machine collaboration mainly from the creative point of view - how will this change the way we work as creatives? I always see AI not as a replacement, but more as an extension of our abilities.

Other topics like no nation-state, world governance, universal income, the welfare state and the gender decisions are all topics I am already really connected to. 

I would have liked to have more room and time to discuss these projects and also not only work on speculative futures but also on design proposals which could address different aspects of problem-solving. [Here](https://www.designit.com/work/designing-out-waiting-times) is one example of design solution thinking that I think we and the course could focus more on. 

It is a very [economical approach](https://www.mckinsey.com/industries/high-tech/our-insights/the-strength-of-weak-signals), but weak signals help to create possibly design solutions ahead of upcoming conflicts or trends which in my opinion is a core element as a designer for emergent topics.

&nbsp;

## Speculative Future Scenarios.

In every weak, we had to present a future scenario based on the weak signals which were introduced to us the week before. Here I present a selection of the group submissions I was part of and which are related to my master project or influenced me on my general future perspective.

&nbsp;

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT7_eep4SfKwSGUgOfqhK-UwX_PezZQRj4ZhZ0umx-fnyqybSK5-VMSBncb1rlUpUukKcA1Mb4lEuvO/embed?start=false&loop=false&delayms=3000" frameborder="0" width="740" height="445" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## final presentation: Human-Machine Interaction and The Data Theft Alliance.

For our final presentation, we created a science fiction movie where we have no nation-states anymore. The world is governed by corporates. Life is decided by algorithms. A whistleblower sends out his message with hidden tools and forms an underground movement which uses artificial digital copies of personalities to manipulate to corporate algorithms. We created a trailer, what I can't embed as not all material we used is royalty free. But I can send a private link anytime on [request.](mailto:gabor.mandoki@iaac.net)

&nbsp;

&nbsp;

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ7KR94VX_9wUaNs-nlu5RSiJ_UKUM9JYiTONX9mWJMq_e4z2gRuarm7bN0Ym6JBPWILf4uCOg2P_O8/embed?start=false&loop=false&delayms=3000" frameborder="0" width="740" height="445" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

If you want to download the picture in a higher, printable resolution, click [here]({{site.baseurl}}/assets/atlas/timesquare without adds print.tif).

## A world without ads. 

In the first week, the main topic was called Surveillance Capitalism. My group and I focused on manipulation and identified advertising as one of the main tools for manipulation. So we created a world without ads. As much as I enjoyed the project, the outcome wasn't the greatest as we got on the wrong track with discussing and combining different utopian views on the world. But I learned a lot about the different perspectives on manipulation and the opinions about Surveillance Capitalism. 

The methods used to manipulate us is one of the core motivations to do my master projects. 

&nbsp;

&nbsp;

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTo3Idqj3oDL6YSKx3xQ7aX2BVtR3NuHKXFNFDZsDDg_-EZXoIH7L5tmiBTIvuVcDf8ECGxsO4cVRvz/embed?start=false&loop=false&delayms=3000" frameborder="0" width="740" height="455" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## The Albert Institute.

The Albert Institue is the project we created in the Anthropocene week. It describes a way to use the analysis capabilities of an AI system we already know from finance and insurance products, to forecast possible outcomes of governmental actions and to use this as an instance of evaluation to choose the right action. This project covered my interest in politics more than a direct connection to my master project. 

&nbsp;

&nbsp;

![]({{site.baseurl}}/assets/atlas/human-machine-creative-collaboration-pres.jpg)

## Sonar 2050.

For the week Future of Jobs, we focused on human-machine creative collaboration and music. It was very interesting with my background as a musician to analyse the status quo and to see where it will probably head to. We sketched up a scenario of Sonar 2050 where the artist plays together with AI musicians and that the crowd has also the ability to participate in the performance.

In this specific case, my conclusion is a bit different from what we were talking in class. Yes, a lot of jobs will be replaced by AIs, but there are jobs and passions, for example in the creative field, where it is simply not necessary to replace humans. We will use AI systems as assistance and lower the barriers "to be creative". And for sure some cheap background music or stock photos will be replaced but in general, I think we should really rethink if humans really have the approach to replace humans in creative fields. My proposal is, AIs will be instruments and brushes with more complexity but the impulse will be given from humans.

&nbsp;

## Methods.

### Mapping.

One method to create relations between week signals was to map them out as shown in the picture below:

![]({{site.baseurl}}/assets/atlas/WeakSignals-14.jpg)

After defining the main topic, we tried to create an atlas to show relations between signals in this topic. We used the same method the next day as a foundation for the Albert Institute submission. 

&nbsp;

### Data scrabbing. 

One of the methods we learned to gather data related to topics, was data scrabbing. We used Python and different libraries to get the keywords from articles we found due to our research to the main topics. 

I took the challenge to automate the process and to add the scrabbed keywords into a Google data sheet.

&nbsp;

#### tutorial. 

To set up everything, watch [this video](https://www.youtube.com/watch?v=7I2s81TsCnc&t=881s) first to understand how to access the sheets from google. I would also recommend creating a separate sheet and not to work in the actual sheet just to make sure you are not overwriting some data. 

You need a Google API and an Algorithma API (we already had that in class when we were scrabbing some data from pages with Lucas)

```python
#install all required libraries

!pip install gspread 
!pip install oauth2client
!pip install PyOpenSSL
!pip install algorithmia

#find out where the root folder of your program is and then copy the credentials .json file to that space
import os

print("Path at terminal when executing this file")
print(os.getcwd() + "\n")

import gspread
from oauth2client.service_account import ServiceAccountCredentials #will use the credential file downloaded before to auth
import os.path
from bs4 import BeautifulSoup as bs4
import urllib
import pprint
import Algorithmia
import ast
client = Algorithmia.client("api-key") #replace with your own API key



def url_key_words(url):
  input = {
    "url": url
  }
  algo = client.algo('harikrushna16/website_keywords/0.2.0')
  #algo.set_options(timeout=300) # optional
  #print(algo.pipe(input).result)
  keywords = algo.pipe(input).result
  return keywords



scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive'] #these are the APIs we were adding before

credentials = ServiceAccountCredentials.from_json_keyfile_name('SpreadSheetKeywords-XXXXX.json', scope) #directorty to the file you were uplading

gc = gspread.authorize(credentials)

wks = gc.open('nameofthespreadsheet').sheet1 #name of your spreadsheet

headers = wks.row_values(1)

#the number of the collums you want to add the keywords, twitter Bios and have the URL. In this case A=1, B=2, C=3 and so on...
URLCol = 4
keywordsCol = 5
twitterBio = 6

val = wks.col_values(URLCol)
headers = wks.row_values(1)
print(headers)


cell_list = []


for x in val:
      # get keywords from url
      keywordsRaw = url_key_words(x)
      
      if keywordsRaw is None:
        pass
      elif ('twitter') in x:
          quote_page = x
          response = urllib.request.urlopen(quote_page)
          soup = bs4(response, 'html.parser')
          content = soup.find('p', attrs={'class': 'ProfileHeaderCard-bio u-dir'})
          cleaned = content.text.strip()
          cellWithUrl = wks.find(x)
          cellToUpdate = wks.cell(cellWithUrl.row, twitterBio)
          cellToUpdate.value = cleaned
          cell_list.append(cellToUpdate)
          wks.update_cells(cell_list)
      else:
        keywordsDict = ast.literal_eval(keywordsRaw)
        keywords = str(keywordsDict['keyTerms'])
      
        # find a cell to update
        cellWithUrl = wks.find(x)

        # get the cell to update
        cellToUpdate = wks.cell(cellWithUrl.row, keywordsCol)

        # update cell value
        cellToUpdate.value = keywords
        cell_list.append(cellToUpdate)
        wks.update_cells(cell_list)

```

You can also try yourself in the [MDEF notebook](https://colab.research.google.com/drive/1V0hu3gFlwezWxzxSTrm7n4u2s_gycmbh). 

&nbsp;

### Reflection on methods.

Both methods helped me a lot to put different aspects into a relationship. It ended up using both methods to map out the weak signals used for my master project on [kumu.io](https://embed.kumu.io/3ad6defa64abaab4ad124774dedf5442). I used my code to automatically gather the keywords, cleaned them up and feed them into kumu. Now I can look for relationships between the topics and continue adding and mapping. 

&nbsp;

### Overall refelction.

I really enjoyed this course as it addresses the core element of what it means to create for emergent futures: understanding the future, speculative thinking and to define the upcoming. 

I would recommend moving this course to the first term as it can be really helpful to combine research methods, speculative futures, design proposals and the workshops we had term one to one big tool for defining the area of interest. Then in the second term, the focus could be more on how to create design proposals.