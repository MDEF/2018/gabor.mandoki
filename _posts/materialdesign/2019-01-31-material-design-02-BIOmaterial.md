---
layout: post
title:  "material design 02: BIOmaterial."
date:   2019-01-31 10:00:00 +0530
categories: materialdesign term2

---
![]({{site.baseurl}}/assets/term2/materialdesign/biomaterial/BIOMaterial presentation.jpg)


# Spent Grain.
---

## What is spent grain?

- Spent Grain or draff is the main by-product of the beer brewing process
- depending on the type of beer, it can be out from different kinds of grain
- the main type used for brewing is barley
- the residue of malt and grain remaining in the mash-kettle after the mashing and lautering process
- consist of grain husk, pericarp & fragments of endosperm
- main containments are carbohydrates and proteins

&nbsp;
&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/biomaterial/BIOMaterial presentation3.jpg)

&nbsp;

## Brief history of beer.

- one of the oldest & most widely consumed  alcoholic drinks worldwide
- first records going back to 5000 BC in Iran
- beer making was a home activity in the middle age in Europe
- the brewing process was cultivated for a bigger industry in the 14th century
- 1516 the “Reinheitsgebot” (purity law) was adopted in Bavaria and ordered the ingredients of beer are only water, barley and hops (yeast was added later) - that makes spent grain a very clean material
- makes it the oldest food regulation
- most European breweries have a strong tradition from back in the days
- the new craft beer culture is braking with these old traditions

&nbsp;
&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/biomaterial/BIOMaterial presentation4.jpg)

&nbsp;

## What is spent grain used for?

- baking bread
- can be used as an ingredient for cooking (Brewers Schnitzel)
- food for wildlife
- food for cattle cows
- biogas production

### update:

An addiotinal very interstinf project is [beer filament](https://eu.3dfuel.com/products/buzzed-beer-filament).

&nbsp;
&nbsp;

![]({{site.baseurl}}/assets/term2/materialdesign/biomaterial/BIOMaterial presentation2.jpg)
