---
layout: post
title:  "design for the real digital world."
date:   2018-10-22 19:45:31 +0530
categories: [term1]
---
<iframe src="https://player.vimeo.com/video/297562466" width="740" height="360" frameborder="0	" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

# design for the real digital.
---
&nbsp;


### the task.
The task for this week was to redesign our workspace. We were supposed to only use the material we found on street in Poblenou to build new elements with the tools from the fab lab and the technique of digital fabrication.

We started collecting in the first week of the master, so over the time, we gathered a lot of very diverse objects and materials.

### our idea.

After we joint together into four groups, we defined our needs and atmosphere we want to create for the classroom.

We named our concept

&nbsp;

### MDEF Plaza - a dynamic place with multiple uses and spaces  with nature and relaxation and also with individual and collaborative workspaces.

&nbsp;

We found keywords for this room

&nbsp;

### nature. relaxing. inspiring. collaborative. social.

&nbsp;

**… divided the room into the following zones…**

![]({{site.baseurl}}/assets/week3/jpegs/floorplan.jpg)

**… and created some drafts showing how the zones could look like.**

![]({{site.baseurl}}/assets/week3/jpegs/bwsketches.jpg)

**Additionally, we developed some tools like a movable separator, beanbags and a recycling space.**

![]({{site.baseurl}}/assets/week3/jpegs/reciclyng.jpg)

**With our agenda defined, we’ve been going through the material and started to build some pieces digitally.**

![]({{site.baseurl}}/assets/week3/jpegs/renderingsOverview.jpg)

After presenting all ideas, we discussed collectively which projects should be realized. Our group got the task to build the stuffLab, recycling space, movable separator, and the beanbags. So we divided our group into smaller teams of two and started with the stuffLab, beanbags and the separator.

&nbsp;

### the week.

Katherine and I worked on the separator. We first collected all pieces we would like to use, measured them and built a 3D model of the wall.

Together with Ingi, we discussed different methods to bind the different parts together. In the end, we decided to use glue to make sure to keep the wall as light as possible but still solid.

As a base, we used a heavy old door with some wheels on it.

We 3D printed some hooks to have the ability to hang jackets and used the CNC to cut a shape we designed into some parts of the wall as well as to cut the parts for the side of the frame to fix the wall on the base.

The final assembly happened in the classroom. Otherwise, we wouldn’t have been able to bring the wall up to the second floor.

![]({{site.baseurl}}/assets/week3/jpegs/Seperator.jpg)

Meanwhile, the rest of our group mainly worked on the stuffLab.

![]({{site.baseurl}}/assets/week3/jpegs/stuffLab.jpg)

&nbsp;

### map.

![]({{site.baseurl}}/assets/week3/maps/APC_0620.jpg)

After redesigning the class, the map we had to highlight the most important places around IAAC didn't really fit anymore to the overall look.
So, I decided to create a new one by engraving a piece of wood with a map of Poblenou.
As it turned out, it wasn’t that easy to find a clean map on the web I could use for the job.
Consequently, I had to take a picture of a map I liked and trace the outlines in Illustrator to create a vector file. The created files are now available on this page for anyone to download.
After creating the map in illustrator, I was helped to create the right file with Rhino for the laser cutter software. I used a piece of wood we found on the street and cut it into the right dimensions for the laser cutter (1000mm x 600mm).

**downloads. (right click, save file)**

[Illustrator file](https://mdef.gitlab.io/gabor.mandoki/weeks/assets/week3/maps/mapPobleNouMDEF.ai)

[EPS file](https://mdef.gitlab.io/gabor.mandoki/weeks/assets/week3/maps/mapPobleNouMDEF.eps)

[SVG file](https://mdef.gitlab.io/gabor.mandoki/weeks/assets/week3/maps/mapPobleNouMDEF.svg)

[PNG file](https://mdef.gitlab.io/gabor.mandoki/weeks/assets/week3/maps/mapPobleNouMDEF.png)

&nbsp;

### the conclusion.

In this week I learned a lot about the machines available in the FabLab and the tools and methods of digital fabrication as I never used a CNC, laser cutter or 3D printer before. However, I am really looking forward to diving deeper into this field.  

I think that working with the already existing material made this project very special, as you have to stick to more or less to what is available.
As a designer, you are used to starting with the design and not limit yourself with the material. In this case, the design followed the raw material and not material the design.
