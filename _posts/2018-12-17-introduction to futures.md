---
layout: post
title:  "Intrdocution to Futures."
date:   2018-12-17 15:00:00 +0530
categories: [term1]

---

![]({{site.baseurl}}/assets/week10/david-di-veroli-3639-unsplash.jpg)

# An Introduction to Futures.
------

&nbsp;

### The avocado & mango utopia.

*In 2050 the world is separated into two parties. The mangoiests and the avodiests. The avodiests live in the south of the earth, a dry desert land. The mangoists habitat is the north, a tropical jungle.*

*Both parties were grown from the radical consequences of climate change and invented their own strategies to survive. In the year 2025, it was clear that the only chance is to focus on one source for food and energy and to biohack to serve all demands. There have been two options. Avocado or Mango.*

*In the north, in the humid tropical area, the people decided to put all one's eggs into one basket - the mango. They hacked the fruit and developed all kind of varieties. Not only sweet or salty mango but also mango you could generate fuel from or produce fabric.*

The southern part did pretty much the same but with avocado.

*They both also invented new currencies based on the value of the fruits. The avocoin and the mangocoin. If you were good at farming or the quality of your avocado was good, you were able to earn coins. Because these coins values were attached to the value of the fruits, everyone had the approach to keep and improve the quality of them.*

*All this could only happen because humanity trusted a supercomputer which calculated this scenario. With all the input of the crises and climate change, the result was to divide into two parts with two fruits.*

This example showcases in a really simplified way what we learned this week. Elisabeth gave us an insight into what future studies is about. It is a scientific method to envision the future. The focus hereby is on different steps to create an abstract from the present and the past as the humans are influenced by what they already know and that limits their imagination what futures could look like. It is also clear that the future can't be really predicted as it is simply not existing.

I was wondering in the end if futures studies give you tools to understand the future more or less to handle it or does it have the approach to change it? Is it changeable only because we know it?

The idea of an unattached imagination being more realistic than the consequence of the present seems to be a real challenge. Even though the example before only plays with factors we know from today. Climate crises, deserts, jungles, fruits, biohacks and supercomputers... nothing completely unrealistic.

### conclusion.

What I learned this week for my final project and in general is that I will focus more an alternative future scenarios and so create not only a concluded picture of the present but also a more challenging and on the same level truthful (or in this case not) future the will enlight about the usability and robustness of my ides for the emergent futures.  

<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/4gi5nU2Yz5jvG" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/cheezburger-explosion-building-4gi5nU2Yz5jvG">via GIPHY</a></p>

### the controlled detonation of the present.

Our wealth in the western world is built on a shaky construct. Experts from different fields agree on that the next financial and economic crisis is around the corner. But why is that? Haven't we learned from the crises ten years ago?

The banking system is still the same. We flooded the market with uncovered money. The stock market is unattached to any real market. The banks are still betting on bad papers. So obviously we haven't learned. The regulations the politics promissed haven't been made. The consequence is a huge crash we are heading to. Some say the impact of the last crash was only a hint compared to the next one.

So what can we do about it? I believe that we have to create an alternative future without the traditional banking system. We have to build a transparent, decentralized communal financial system which is attached to real values rather than hypothetical ones. And when we are done with that, we should force a controlled detonation of the old system with the new system we are having up in our sleeves.

Each crash in history was not only a crash and real suffer mainly for the poor ones in our society but also a chance to renew. As we didn't learn from the last crash as it seems and the next one is unavoidable, we should really work hard on alternate futures.

There are ideas and technologies we can use. Cryptocurrencies are only one popular example. But having this in mind, our government and society should realise that with future studies and alternate futures we can prepare solutions and answers for seen or even unseen events to improve more than to conserve our old habits.



􏰘􏰧􏰓􏰌􏰐􏰑􏰐􏰓􏰔􏰛
