---
layout: post
title:  "Design Dialogues."
date:   2018-12-26 15:00:00 +0530
categories: [term1]

---

![]({{site.baseurl}}/assets/week12/screenrecording mycroft_highquality-0001.jpg)

## Emanuel Project.

#### An Intervention in the daily routine to create awareness.

*click here to read the [PDF](http://mdef.gitlab.io/gabor.mandoki/assets/week12/porjectEmanuel_journal.pdf)*

---



### My perspective of the future.

My vision of the future relies on awareness, responsibility and a horizontal power structure in our society all driven by education, information and technology. My all in all approach is to create a portfolio of tools promoting transparency, privacy and awareness about the environment of each and every one of us.

&nbsp;

**Transparency.**

After we missed the chance in the times when radio, tv or the internet were new technologies to decentralize the suppliers of information we now have a new chance to do so. There are new technologies. If we use them right, it could lead to a transparent, decentralized world with spread power to every one of us. A lot of trouble is caused by the hidden figures and deals behind the curtains in politics and economy. People are losing trust in the so-called “establishment” and shifting to extreme political sides. I believe that there is no need to keep things secret in the public field - but only if everything is transparent, like an open-source government. A good example of this is the data.gov.uk project of the UK that provides open-data sets from different departments of the Government. Since 2010, the platform contains over 30.000 datasets accessible to everyone.  

&nbsp;

**Privacy.**

As much as I believe in transparency, I also believe in privacy. For being transparent in production chains, politics or finances no-one has to give up privacy. We empowered companies by giving them our data for services which gains the feeling that life wouldn’t work out nowadays without giving up privacy. We should promote to get this power back to us and be aware of deciding what we want to give and what not. Different people and companies like Rand Hindi with the project “snips” or Tim Berners-Lee with “Solid” are working on this idea. This emerges to be the next step to regain control over data and information.   

&nbsp;

**Information.**

The majority of our society is not aware of significant things happening around them. Not aware of the consequences of their daily routine. Not aware of what they are eating. Not aware where they leave personal information. Not aware of the complex machinery behind the echo chamber creating information monopolies. We all got used to prepared and portioned information dishes - and of course only dishes we enjoy! We gave up responsibility. But information is everywhere. There will be new ways of providing this information. From different sources, in the right moment, at the right spot and in the right context. We can create a new level of awareness in politics, economy, society down to our day to day behaviours.

&nbsp;

**Intervention: Daily routine & household.**

I want to focus first on the daily routine and the household. This is a small but complex environment which also allows me to test everything first on myself. The next step would be to bring this outside to work, sport, shopping etc. Creating awareness of the daily routine and the consequences of the everyday habit is my approach with this project.

&nbsp;

### State of the art.

&nbsp;

**Mycroft.**

Mycroft provides an open source voice assistant. They developed not only their own hardware but also an OS called Picroft you can add to your Raspberry Pi and build your own assistant. I also tried this system. But in this case, I didn’t use it as a voice assistant. I connected it to a Telegram bot and started to chat with it. The experience wasn’t really convenient. Some skills it supposes to have were not working. Some information has been too specific. But it was very interesting to see how an open source community is adopting such a project.

&nbsp;

**snips.ai**

Snips is a voice assistant project founded by Rand Hindi. They call it themselves a private-by-design, decentralized voice assistant that makes technology disappear.

I tried it myself by setting it up on Raspberry Pi. You can compile your own assistant on their homepage with different skills and then upload it to your Pi. If you are used to working with the command line, the installation is easy. The quality of the voice engine wasn’t really convenient which only proves that this project is in an early stage. It is planned to release a beta version of their own hardware early 2019.

 &nbsp;

**Solid.**

Tim Berners-Lee, the founder of the www himself, wants to reform the today’s internet to what it originally supposed to be. For that, he is promoting the project “Solid”. Solid is an open-source platform developed over the last 15 years which should empower the user again to control their personal data. He attacks today’s issues with privacy and data protection on the internet.

&nbsp;

**data.gov.uk**

data.gov.uk is a project by the UK government launched 2010 to make non-personal open data accessible. In the meanwhile, the platform contains over 30.000 datasets from different departments of the UK government.

&nbsp;

**Open Data BCN.**

Open Data BCN is a project of the city of Barcelona that has the approach to increase the control of the citizen over their data. The companies working for the city authorities are asked to provide their collected data to this database to make privately collected data accessible for the public.

&nbsp;

**Replika.**

Replika has the approach to provide you with a friend that helps you to reach goals like to eat more healthy food or do more sports. It is an AI what is communicating with you daily over a chat app on your phone. I tested it for a few days. My AI, Dan and I had really natural conversations. It asked me a lot of questions to paint a picture of my personality. It reminded me every day to talk to it and tried to give me a good feeling. In the end, it didn’t catch me really and I have my concerns regarding my data as I couldn’t find out what is happening with it.

&nbsp;

### Fields of interest.

**AI & assistance.**

Artificial Intelligence for me is one of the most promising fields in technological development. It allows us to create machines that can assist us in more or less rational areas of our life. I believe that we will end in a scenario where everyone has the ability to have his personal artificial assistant that supports you in your daily routine. What the development of AI already does, and I am sure this will be more than less in the future, is challenging our understanding of moral and ethical principles. Can we generalize ethics to all humans? Can technology take these questions from us and answer them? Is a super-intelligent machine the answer to all asked questions of mankind? Probably not, but maybe it can help us to find the answer ourselves. We had a chance in the week of Designing with Extended Intelligence to dive deeper into this field. We were challenged to develop an assistant for everyday tasks or to face certain issues of our society. These exercises and the ideas developed over this week inspired me to focus on this field.

| **book:**    | *“The Digital Ape: How to Live (in peace) with Smart Machines”* by Sir Nigel Shadbolt and Roger Hampson. |
| ------------ | ------------------------------------------------------------ |
| **project:** | snips.ai, Mycroft - both AI systems with the focus on privacy and open-source. |
| **project:** | Replika - a chatbot that behaves like a friend and helps you to improve. |

&nbsp;

 **Human-Machine  Interaction & interface**.

We interact with machines every day. And it’s getting more and more. There are different approaches and some of them are so popular, that they feel very natural, like our touchscreens. I want to have an interface for my machine that interacts with the user in the most natural way possible. That means to use already existing interaction surfaces to make it as easy as possible to integrate it in the daily routines and to avoid the requirement of learning new interaction methods. The machines should give the user the freedom to decide if and when to interact with it - a shadow existence only visible in the right moment.  

| **book:**    | *“The best interface is no interface”* by Golden Krishna.    |
| ------------ | ------------------------------------------------------------ |
| **book:**    | *“Hooked”* by Nir Eyal.                                      |
| **project:** | HyperSurfaces - turning any surface to an interface of interaction with deep learning and vibration sensors. |

&nbsp;

**Privacy & technology.**

Technologies to regain the power of your own data are on the rise. Encryption, blockchain and VPN are only the most popular in this filed. All projects of the portfolio should include privacy and diversity of sources for information by design. This principle generates trust and security. All personal data is in the hand of the user and completely encrypted on-device. To reach that approach, I will occupy myself with the possibility to develop on the Solid platform of Tim Berners-Lee that allows the user to control its data.

| **article:** | Interview with Rand Hindi on wired.de about the snips.ai project, Google and other data collectors and how to design for privacy. |
| ------------ | ------------------------------------------------------------ |
| **article:** | *“One small step for the web”* by Tim Berners-Lee in which he announces the Solid project he worked on for the last 15 years. |
| **article:** | *“How Google Tracks Your Personal Information”* by Patrick Berlinquette on Medium. |
| **project:** | Solid - a platform that restores the power of the individual on the web |

&nbsp;

**Information & data.**

What is information? Where is information? What transports information? What kind and amount of information can we handle? We got used to the overflow of information hitting us every day. Even if it’s filtered, which is most of the time, I, for example, receive around 200 notifications on my phone every day. And there is way more than only the phone… How should we handle this amount of information? Is the quantity or the quality an issue? The filters we use today are driven by commercial interest rather than your preferences. The algorithms of Facebook, Google and co. only show things fitting into our bubble without challenging us and our opinions. We have to ask ourselves if we want to leave the importance of providing information centralized with this companies or if we want to do something about it. The solution could be a personal assistant that helps you to organize information and news and evaluates the quality and the credibility of it. It works only for you and is only driven in your interest.

| **article:**      | *“Don’t want to fall for fake news? Don’t be lazy”* by Robbie Gonzales on wired.com. |
| ----------------- | ------------------------------------------------------------ |
| **book:**         | *“Outnumbered”* by David Sumpter.                            |
| **short  story:** | *“Bubble Universe”* by Andrew Hickey.                        |

&nbsp;

### the experiment.

I want to create an assistant for awareness by delivering information about your daily routine in the most neutral way. The assistant focuses on getting the information you ask for, adds background research and collects this information in one place you can access anytime. Preperation.

&nbsp;

####**Preparation.**

**What do I want to know?**

To set up an experiment in this field, I had to define what things I want to know, what things I do not know and what things I don’t want to know. I am a very curious person, so this task was relatively easy. I’ve tried to collect questions in a notebook regarding my daily routine to get an idea of the amount of information the assistant had to provide.

 **What provides information?**

The next thing I was wondering about is, what is already providing information? Texture, smells, sounds, visuals? All this and more provides information we understand directly. How can we use this? Can we add context to this information? How do we add context to this information? Could augmented reality provide this context to raw information?

**What interface is the most convenient in terms of usage and inconspicuousness?**

To find out which way of interaction is the most convenient for me, I tried different kinds of methods.

First, I used Siri with my AirPods which turned out to be nice whenever you are walking, or you are alone. Hands-free, only with your voice. But in public, like in the metro, it felt really awkward to talk to her.

The next thing I worked with was Replika, a chatbot that behaves like a friend and has the approach to motivate you in a daily dialogue to reach goals like to do more sports or to eat healthier. The interaction itself felt more natural than to talk to Siri as the misunderstandings were reduced to a minimum and it felt more private. The only problem I had was that it also felt very awkward to chat with a bot that behaves like a friend from home but wasn’t.

My conclusion is that I focus more on chat than voice. As I experienced, reading what a machine wants to tell you feels way more natural compared to the synthetic voices of today’s assistances, but the machine should still be recognizable as a machine.

The next thing is, you can’t decide if you want to hear something. But you always can decide if you want to see something. Considering this circumstance for interfaces, the interaction via text gives you more freedom if and when you access the
 demanded information.  

**Example**.

I created an example situation where I showcase how my assistant should work:

*You wake up and go to the kitchen to make a cup of coffee. You realize that you don’t know what coffee actually is. You just ask:*

**“Emanuel, what is coffee?”**

*Emanuel will now send a notification to your phone. You can read it at the same moment or keep it for the ride to work.*

This example shows how it could be possible to use an assistant to generate a list of things you are curious about. Other examples could be:

&nbsp;

**“How to divide the trash right?”**

**“Where do the materials for my phone battery come from?”**

**“How much carbon dioxide could  I safe by showering one minute less?”**

&nbsp;

####**Prototype.**

**Build & try an assistent.**

The next step was to build this kind of assistant by myself. I got a Raspberry Pi, a microphone, a speaker and two different platforms I wanted to work with.

The first one is called snips. Snipes is providing a web-based toolbox where you can click together the skills for your personal assistant. I did so and uploaded everything on my Pi. It only provides a voice assistant, but they have already announced to release a chatbot soon.

My experience with „Jarvis” (the name of the AI from snips) was frustrating. It was nice and easy to set it up. But it didn’t hold what it promised. It turned out that snips are more focusing on a smart home assistant than an assistant to interact with as the voice of the text to speech synthesizer was really bad and it wasn’t even capable of reading an article from Wikipedia. I will give it another run as soon as they release a new version. But right now, it is not really usable for my case.

The second platform I tried was Mycroft. It is an open-source platform where you can build your assistant in a similar way. Compared to snips it was not so easy to set up. But as it seems it was capable of lot more things. In the end, it didn’t work out as a voice assistant as the hardware configuration I used for snips wasn’t compatible with Mycroft. So, I hooked my machine up to Telegram and so I was able to chat with my machine. The experience was much nicer than with snipes.

The machine had a proper answer to the asked questions. A lot of facts gathered from different sources. Mycroft was way more on the path I could imagine as an assistant.

**Conclusion.**

This experiment showed me that there are solutions from smaller, privacy focused companies that allow you to build your own assistant. But these assistances are at an early stage. They have to improve a lot to go on the battlefield against the few big players like Alexa, Siri, Duplex or Alibaba’s AI.

####**Next steps.**

The next steps for my project will be to test more of the many existing AIs which could fit for my purposes. Besides the system, I will focus on how to access available information like Wikipedia or any news channel to use them as a provider. How can I guarantee the quality of information? Can I use an AI to provide that? I will also consider enlarging my field of intervention from the household to outside like the university, sports, shopping etc. I will also do my research on how the atomize gathering information with a monitoring system for the daily routine. There is a project called synthetic sensor by Gierad Laput which allows you to monitor what you are doing in every room. In addition, all of us have a complex sensor in our pocket which can be used for that purpose. I could also imagine taking the Smart Citizen project as an example to take it into our homes which observes your energy consumption, the number of radio waves you are exposed to or how much waste you are producing.



### reflection & feedback.

After the final presentation, my own reflection was that I didn't point out enough that the AI I was working with was only an experiment to see what tools could help to work on my future vision as well as to showcase the status quo of such projects. Most participants focused too much on the product itself and less on my global approach - which obviously was caused by the way I presented my project as the "prototype" was featured the most.

And so the main feedback of the jury was that I should create more scenarios where to use my designed assistant. Also in which context I see this in the future. I also was asked what I would call my assistant in terms of is it a friend like "replica" or is it more a secretary etc.

I was also asked a lot how I could realise this machine with my approach to privacy as the big players nowadays are miles ahead also because of their access to a huge amount of data.

### next steps.

I will work on the connection of my project/product to my overall vision to the future to make it more clear what I am up to. I will also shift my focus from the prototype I have right now to a more tangible example, as many were confused by the application of the machine. I will also rethink the machine-human interaction aspect. I was thinking about the Star Wars saga, as it has drawn the perfect symbiosis between humans and machines. While preparing my final presentation, I also figured out that I would like to put my focus on a decentralised information system that has the approach to challenge the user and so to
