---
layout: post
title:  "fab 14: Network & Communication."
date:   2019-05-08 10:00:00 +0530

categories: fabacademy


---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week14/BTSerial_monitor.2019-05-08 21_24_59.gif)

# Network & Communication.

------

&nbsp;

For this weeks submission I partnered up with Oliver. Our aim was to connect a 10k potentiometer we bought in China to a ESP32 board and send the Serial values via Bluetooth (BLE) to another device.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week14/Fritzing sketch.png)

## Connection.

We connected the potentiometer to GND, 3V3 and the pin called VP, which is one of 6 analog read pins on the ESP board we used. [Here](https://circuits4you.com/2018/12/31/esp32-wroom32-devkit-analog-read-example/) you find the pinout and a description on how to read analog signals with the ESP32 DevKit1.

Now we used to code to analog read from the pin and uploaded the code.

We used following settings in Arduino IDE:

**Board:** ESP32 Dev Module

**Upload Speed:** 115200

If you can't find the ESP Dev Module you have to install the library for it. For that open up preferences. In the bottom part you will find a section where you can paste a URL. Copy and paste this URL  which will add the ESP32 boards to your library.

`https://dl.espressif.com/dl/package_esp32_index.json`

We used following code. While uploading, you will probably have trouble to connect to your board. After compiling, Arduino will look for the device. Press the "BOOT" button for few seconds and release again. Than it should connect.

```c++
// These constants won't change. They're used to give names to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
}

void loop() {
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);
  // change the analog out value:
  analogWrite(analogOutPin, outputValue);

  // print the results to the Serial Monitor:
  Serial.print("sensor = ");
  Serial.print(sensorValue);
  Serial.print("\t output = ");
  Serial.println(outputValue);

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);
}
```

When successfully uploaded you can open the serial monitor and  check how the values are changing. This set up, we now want to send the values over a BLE connection.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week14/photos/IMG_5182.jpg)

## Adding Bluetooth

We use the BluetoothSerial library following [this tutorial](https://techtutorialsx.com/2018/03/13/esp32-arduino-bluetooth-over-serial-receiving-data/). You fill find it in your library manager if you don't have it. This library works similar to the serial commands you already know.

```c++
#include "BluetoothSerial.h"

BluetoothSerial SerialBT;

const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to


int sensorValue = 0;
int outputValue = 0; // value read from the pot


void setup() {
//give your device a name you like
  SerialBT.begin("long names work now");
}

void loop() {
   // read the analog in value:
   sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);

  SerialBT.print("sensor = ");
  SerialBT.print(sensorValue);
  SerialBT.print("\t output = ");
  SerialBT.println(outputValue);
  delay(500);
}
```

You will see that we used the code from above but added the bluetooth framework. The result is very simple. After uploading, Oliver opened the bluetooth device manager on his Mac, connected to our board via bluetooth, went back to Arduino, selected the bluetooth board and opened the serial monitor or plotter.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week14/screenshots/Screen Shot 2019-05-08 at 14.13.57.png)

## Connecting two ESP32

The next goal was to set up a second ESP32 to receive the sensor values from the first board. To make that happen, we have to set these two boards up as server and client. You can follow this [documentation](https://randomnerdtutorials.com/esp32-bluetooth-low-energy-ble-arduino-ide/) to get a general overview about what client and server means in this context.

We had to generate an [UUID](https://www.uuidgenerator.net) that lets you identify your bluetooth device. We set up the server. For that we just opened the example-sketch from the Arduino menu called BLE_server, added our IDs and our code from before.

```c++
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include "BluetoothSerial.h"

const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;
int outputValue = 0;

BluetoothSerial SerialBT;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID        "49082f8a-479c-47fa-9246-6188dd7d495a"
#define CHARACTERISTIC_UUID "f5f83421-28a0-4aaa-979b-8dfd6731e120"

void setup() {
  Serial.begin(115200);
  Serial.println("Starting BLE work!");
  SerialBT.begin("ESP32");

  BLEDevice::init("Long name works now");
  BLEServer *pServer = BLEDevice::createServer();
  BLEService *pService = pServer->createService(SERVICE_UUID);
  BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic->setValue(sensorValue);
  pService->start();
  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is working for backward compatibility
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  Serial.println("Characteristic defined! Now you can read it in your phone!");
}

void loop() {
  // read the analog in value:
   sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);

  SerialBT.print("sensor = ");
  SerialBT.write(sensorValue);
  SerialBT.print(sensorValue);
  SerialBT.print("\t output = ");
  SerialBT.println(outputValue);
  delay(500);
}
```

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week14/photos/IMG_5184.jpg)

With this set up we created the client. The procedure was similar. We opened the example sketch called BLE_client. Before we added any other code, we just changed the UUID to ours an tested if the two devices would connect. The connection was successful and so we started to add code pieces to read and print incoming serial values. The problem was, that we couldn't define the incoming values to be printed. We have to work on that.

```c++
#include "BLEDevice.h"
#include "BluetoothSerial.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

Adafruit_SSD1306 display(-1);
//#include "BLEScan.h"

int sensorValue = 0;
int outputValue = 0;

BluetoothSerial SerialBT;

// The remote service we wish to connect to.
static BLEUUID serviceUUID("49082f8a-479c-47fa-9246-6188dd7d495a");
// The characteristic of the remote service we are interested in.
static BLEUUID    charUUID("f5f83421-28a0-4aaa-979b-8dfd6731e120");

static boolean doConnect = false;
static boolean connected = false;
static boolean doScan = false;
static BLERemoteCharacteristic* pRemoteCharacteristic;
static BLEAdvertisedDevice* myDevice;

static void notifyCallback(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,
  size_t length,
  bool isNotify) {
    Serial.print("Notify callback for characteristic ");
    Serial.print(pBLERemoteCharacteristic->getUUID().toString().c_str());
    Serial.print(" of data length ");
    Serial.println(length);
    Serial.print("data: ");
    Serial.println((char*)pData);
}

class MyClientCallback : public BLEClientCallbacks {
  void onConnect(BLEClient* pclient) {
  }

  void onDisconnect(BLEClient* pclient) {
    connected = false;
    Serial.println("onDisconnect");
  }
};

bool connectToServer() {
    Serial.print("Forming a connection to ");
    Serial.println(myDevice->getAddress().toString().c_str());

    BLEClient*  pClient  = BLEDevice::createClient();
    Serial.println(" - Created client");

    pClient->setClientCallbacks(new MyClientCallback());

    // Connect to the remove BLE Server.
    pClient->connect(myDevice);  // if you pass BLEAdvertisedDevice instead of address, it will be recognized type of peer device address (public or private)
    Serial.println(" - Connected to server");

    // Obtain a reference to the service we are after in the remote BLE server.
    BLERemoteService* pRemoteService = pClient->getService(serviceUUID);
    if (pRemoteService == nullptr) {
      Serial.print("Failed to find our service UUID: ");
      Serial.println(serviceUUID.toString().c_str());
      pClient->disconnect();
      return false;
    }
    Serial.println(" - Found our service");


    // Obtain a reference to the characteristic in the service of the remote BLE server.
    pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID);
    if (pRemoteCharacteristic == nullptr) {
      Serial.print("Failed to find our characteristic UUID: ");
      Serial.println(charUUID.toString().c_str());
      pClient->disconnect();
      return false;
    }
    Serial.println(" - Found our characteristic");

    // Read the value of the characteristic.
    if(pRemoteCharacteristic->canRead()) {
      std::string value = pRemoteCharacteristic->readValue();
      Serial.print("The characteristic value was: ");
      Serial.println(value.c_str());
    }

    if(pRemoteCharacteristic->canNotify())
      pRemoteCharacteristic->registerForNotify(notifyCallback);

    connected = true;
}
/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
 /**
   * Called for each advertising BLE server.
   */
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    Serial.print("BLE Advertised Device found: ");
    Serial.println(advertisedDevice.toString().c_str());

    // We have found a device, let us now see if it contains the service we are looking for.
    if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)) {

      BLEDevice::getScan()->stop();
      myDevice = new BLEAdvertisedDevice(advertisedDevice);
      doConnect = true;
      doScan = true;

    } // Found our server
  } // onResult
}; // MyAdvertisedDeviceCallbacks


void setup() {
  Serial.begin(115200);
  Serial.println("Starting Arduino BLE Client application...");
  BLEDevice::init("");
  SerialBT.begin("ESP32");

  // initialize with the I2C addr 0x3C
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  

  // Clear the buffer.
  display.clearDisplay();

  // Display Text
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,28);
  display.println(sensorValue);
  display.display();
  delay(2000);
  display.clearDisplay();

  // Retrieve a Scanner and set the callback we want to use to be informed when we
  // have detected a new device.  Specify that we want active scanning and start the
  // scan to run for 5 seconds.
  BLEScan* pBLEScan = BLEDevice::getScan();
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setInterval(1349);
  pBLEScan->setWindow(449);
  pBLEScan->setActiveScan(true);
  pBLEScan->start(5, false);
} // End of setup.


// This is the Arduino main loop function.
void loop() {

  // If the flag "doConnect" is true then we have scanned for and found the desired
  // BLE Server with which we wish to connect.  Now we connect to it.  Once we are
  // connected we set the connected flag to be true.
  if (doConnect == true) {
    if (connectToServer()) {
      Serial.println("We are now connected to the BLE Server.");
    } else {
      Serial.println("We have failed to connect to the server; there is nothin more we will do.");
    }
    doConnect = false;
  }

  // If we are connected to a peer BLE Server, update the characteristic each time we are reached
  // with the current time since boot.
  if (connected) {
    String newValue = "Time since boot: " + String(millis()/1000);
    Serial.println("Setting new characteristic value to \"" + newValue + "\"");

    // Set the characteristic's value to be the array of bytes that is actually a string.
    pRemoteCharacteristic->writeValue(newValue.c_str(), newValue.length());
  }else if(doScan){
    BLEDevice::getScan()->start(0);  // this is just eample to start scan after disconnect, most likely there is better way to do it in arduino
  }
  Serial.println(sensorValue);
  SerialBT.print("sensor = ");
  SerialBT.print(sensorValue);

  delay(1000); // Delay a second between loops.
} // End of loop
```

As you see in the code, we also connected an OLED screen to the board which will be our serial monitor as soon as we find out how to monitor the incoming values. To set up an OLED with the ESP board you can follow this [link.](https://lastminuteengineers.com/oled-display-esp32-tutorial/)

### other useful sites:

This is the option to monitor the serial sent via bluetooth with your phone.

[espressif/arduino-esp32](https://github.com/espressif/arduino-esp32/tree/master/libraries/BluetoothSerial)

This site here also shows a different way to hook up to ESP32 with each other.

[In-Depth: Interface OLED Graphic Display Module with ESP32](https://lastminuteengineers.com/oled-display-esp32-tutorial/)
