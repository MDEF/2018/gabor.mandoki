---
layout: post
title:  "updated: fab 03: CAD - 3D model."
date:   2019-02-06 15:00:00 +0530
categories: fabacademy



---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/renderings/SmartSpeaker_draft_2019-Feb-06_03-59-07PM-000_CustomizedView13969475605.png)

[download 3D model.]({{site.baseurl}}/assets/fabacademy/week3/files/SmartSpeaker draft v13.f3d)

# CAD - 3D model. 

------

## my experience with Rhino & Grasshopper.

First I thought I will stick to Rhino as it is the tool we talked about the most in IAAC and I already got in touch with it during the precourse. So I tried to get it done with Rhino and Grasshopper. My, in the end, was, that it is a really powerful tool but simply not made for me. I enjoyed playing around with it especially with grasshopper to see how the change of parameters changes the object. But in the end, I was looking for something more user-friendly. Coming from a communication design background, I am used to the Adobe CC environment, so something we a similar interface would be nice.   

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/CAD_3D_week_doc16 Rhino and GRashopper.png)

Another argument for me was the bad support for macOS. Grasshopper is still only in beta, what you recognize because of the bad performance. And Rhino itself is still one version behind the one for Windows

## ...and then I tried Fusion360. 

I started with this tutorial:

<iframe width="740" height="455" src="https://www.youtube-nocookie.com/embed/A5bc9c3S12g?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

After watching all three parts of it and following the instructions I decided to try it for my SmartSpeaker.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/CAD_3D_week_doc21 Fusion tutorial.png)

In the end, it took me only one afternoon to create a sketch, render and animate it. That made me decide to stick to Fusion and that I will dive in deeper the following weeks. Here are the results:

### basic sketch.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/01_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/02_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/02.5_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/03_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/04_Fusion360 step.png)

The basic housing of my speaker was really easy to make.  I just started with a box, made nice round edges and cut out the inner part. After I added the inner bezels.

### modifying & adding additional components.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/05_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/06_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/07_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/08_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/08.5Fusion360 step.png)

After having the basic shape, I created some holes for the knob, speaker and buttons. In addition, I added the speakers.

### adding material.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/09_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/10_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/11_Fusion360 step.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/screenshots/12_Fusion360 step.png)

After building a rough sketch, I added some materials like wood and aluminium. 

[download 3D model.]({{site.baseurl}}/assets/fabacademy/week3/files/SmartSpeaker draft v13.f3d)

### result.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/renderings/SmartSpeaker-draft-v12.gif)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/renderings/SmartSpeaker_draft_2019-Feb-06_04-18-08PM-000_CustomizedView11201410797.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/renderings/SmartSpeaker_draft_2019-Feb-06_04-18-47PM-000_CustomizedView3440723936.png)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week3/renderings/SmartSpeaker_draft_2019-Feb-06_04-20-28PM-000_CustomizedView44629916860.png)



 