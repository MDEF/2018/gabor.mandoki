---
layout: post
title:  "fab 13: Application & Implication."
date:   2019-04-18 10:00:00 +0530
categories: fabacademy


---

![by Gabor Mandoki]({{site.baseurl}}/assets/phonefarming/phoneFarm_sketch_2019-Apr-06_08-52-12AM-000_CustomizedView15009465744.png)

# Application & Implication: Phone Farm (ing).

------

&nbsp;

To see the context of this project, read the [documentation]({{site.baseurl}}/masterproject/) of my master project.

## the concept.

The concept for my Fab Academy project is the prototype for my design proposal I made for the master. I want to create a so-called "Phone Farm".

It will be a box with different lockable compartments for phones. In these compartments, the phones will be connected to a network. In theory, the power of all connected phones will be combined and released into a computer cloud. This cloud works like a supercomputer and allows clients to use distributed computing power.

For the prototype, the phones will be charged. The charge will come from a USB hub, connected to a Raspberry Pi. The Pi is responsible to detect if a phone is connected or not. This sensing will allow adding an interactive map of the phone farm.

## the map.

This is a draft of the interactive map displaying the amount of phone connected and projects made possible with it:

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week13/pf_treeposter_wood.jpg)

The LEDs will light up as soon enough phones are connected to have the needed amount of computing power for certain projects (like rendering, neural network calculation or simulation etc).

The small LED on the bottom right shows the number of connected phones.

## the compartment.

The compartments themselves will be lockable, have 3 different connections (lightning, USB-C & micro USB), will have status LEDs and an OLED display that shows the time or the number of coins earned while having the phone locked in.

## materials & components.

For the first draft, I will use plywood I cut with the laser cutter. That allows me to change things rapidly if they do not work out.

### LoFi version.

For the LoFi version, I would use simple key or number locks, no status display on the compartments themselves and will simulate the poster with manual adjustments.

### HiFi version.

For the high-quality version, I would use fingerprint ID to lock the compartments to make sure that only the phone owner can open the compartment. Additionally, I would add a QI charging pad as well in each compartment.

### Coasts of materials. (9 compartments)

- Raspberry Pi **35€**
- Plywood
- regular locks **7.50€ pP**
- fingerprint reader **50€ pP**
- electric locks **15.75 euros pP**
- hinges **10€ pP**
- RGB LEDs **1.75€ p 5P**
- OLEDs **17.75€ pP**
- USB hubs ports **25€ pP**
- triple cables **15€ pP**
- QI pads **10€ pP**

## Question to answer.

What version would make sense for the period of time?

How can a Pi sens and show on what port is what connected?

Do I need additional Arduinos for example if I do the electric lock or for the LEDs?

Is there other material than plywood I could use?

## Things already happening...

Here are some parts of my project I already found:

<iframe width="740" height="455" src="https://www.youtube.com/embed/z-LW33ILyxw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="740" height="455" src="https://www.youtube.com/embed/SMmj_qAbyeM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="740" height="455" src="https://www.youtube.com/embed/OEilz2Cq_xY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>