---
layout: post
title:  "fab 17: mechanical & machine design."
date:   2019-05-30 10:00:00 +0530

categories: fabacademy







---

![]({{site.baseurl}}/assets/fabacademy/week17/box1.jpg)

# Mechanical & Machine design.

---

## Soundscribbler

We allready did this assiment in the first term. So this will be a documantion from term 1.

You will find all files you need to donwload [here](https://gitlab.com/MDEF/scribble-sound/tree/master).

# scribble sound.

### hardware requirements.

- Arduino Uno
- Raspberry Pi
- contrast detecting sensors (we used two [Line tracking sensor](https://www.dfrobot.com/wiki/index.php/Line_Tracking_Sensor_for_Arduino_(SKU:SEN0017)) sensors & two [KY-032](http://sensorkit.en.joy-it.net/index.php?title=KY-032_Obstacle-detect_module) obstacle detect module) - both work.
- DC motor (high torque, low speed)
- laser cut housing (file will be linked here)
- 3D printed spools (file will be linked here)
- 2 poles
- 4 bearings
- 10 spacers

### software requirements.

- Arduino [MIDI libary](https://github.com/FortySevenEffects/arduino_midi_library) by Forty-Seven Effects (installable with the Arduino Libary manager)
- [Hiduino](https://github.com/ddiakopoulos/hiduino) makes the Arduino a native MIDI USB device or using a script that translates the serial input to MIDI
- any sampler on the computer for testing (like [NI Battery](https://www.native-instruments.com/en/products/komplete/drums/battery-4/))
- [Samplerbox](https://www.raspberrypi.org/blog/samplerbox-drop-and-play-sampler/) Open Source sampler for Rasberry Pi

### tool requirements.

- Lasercutter
- 3D printer
- regular tools

## hardware setup.

![images/sensorSetUp_bb.png]({{site.baseurl}}/assets/fabacademy/week17/sensorSetUp_bb.png)

## programming.

### preparing the code for the Arduino.

```c++
 #include <MIDI.h> //inlcuding the Midi libary

 // Created and binds the MIDI interface to the default hardware Serial port
 MIDI_CREATE_DEFAULT_INSTANCE();

// we are using 4 sensors in our setup.

int Sensor1 = 6;
int Sensor2 = 9;
int Sensor3 = 10;
int Sensor4 = 11;



 void setup()
 {
     MIDI.begin(MIDI_CHANNEL_OMNI);  // Listen to all incoming messages

//define the sensors as INPUTs

       pinMode (Sensor1, INPUT);
       pinMode (Sensor2, INPUT);
       pinMode (Sensor3, INPUT);
       pinMode (Sensor4, INPUT);
 }

 void loop() {

    //reading the sesnors values

      bool val1 = digitalRead (Sensor1) ;
      bool val2 = digitalRead (Sensor2) ;
      bool val3 = digitalRead (Sensor3) ;
      bool val4 = digitalRead (Sensor4) ;



// track1 (note C1):
  if  (val1 == HIGH) {
     MIDI.sendNoteOn(36, 100, 1); //(note, volicity, MIDI channnel)
  }
     else  {
     MIDI.sendNoteOff(36, 100, 1);}

     delay(100);

// track2 (note D1):
  if  (val2 == HIGH) {
    MIDI.sendNoteOn(38, 100, 1);
  }
    else {
     MIDI.sendNoteOff(38, 100, 1);
     }  
      delay(100);

// track3 (note E1):
  if  (val3 == HIGH) {
    MIDI.sendNoteOn(40, 100, 1);
  }
    else {
     MIDI.sendNoteOff(40, 100, 1);
     }  
      delay(100);

 // track4 (note F1):
  if  (val4 == HIGH) {
    MIDI.sendNoteOn(41, 100, 1);
  }
    else {
     MIDI.sendNoteOff(41, 100, 1);
     }  
      delay(100);

 }

```

The code for the Arduino was set up to send MIDI signals anytime the sensor sends a signal. The challenge was to figure out what note and on which channel to send.

On the first try, the Arduino send uncountable signals at the same time. So the produced signal was one big hit on the same note. To eliminate that effect, you can add a delay of 100ms. Adjust the delay as much as you want, depending on the frequency you want to send notes. The 100ms are the minimum range between each hit in this case. We tried shorter times but than the signals came to fast in our setup.

The next thing you have to take care of is to switch off the sent signal again. That's why you not only sending MIDI on signals but also MIDI off signals. Otherwise, the tone will just keep on playing as soon as it started to send the first time.

The three digits behind the command  "MIDI.sendNoteOn" & "MIDI.sendNoteOff" are defining the sent Midi note.

| 1st digit                                                    | 2nd digit                                                    | 3rd digit                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| note                                                         | velocity                                                     | MIDI channel                                                 |
| The value that defines which note you want to send (in our example 36 = C1). | This defines the value how "hard" or "loud" you want to send the note. The value has to be from 1 (very silent) to 127 (very loud). In our set up it doesn't really matter as we are only using one sample per input. | You can send on different MIDI channels to different devices or instruments. In our case, we used as usual channel 1. Most software instruments are receiving on channel 1 by default. |

### changing the firmware.

After you set up the code, you have to switch the firmware of the USB chip of the Arduino. The regular firmware sends USB to serial codec which makes it possible to see what the sensors are sending for example. But to send MIDI we have to change to a USB to MIDI firmware. Luckily [Hiduino](https://github.com/ddiakopoulos/hiduino) is providing this.

Download the repo and open the zip file in the downloads folder.

Now bridge the two pins on your Arduino right next to the USB port to reset the Arduino.

Now go to terminal and put in the following commands:

```
$ cd /Downloads/hiduino-master/compiled-firmwares'
```

 Here you selected the directory where the firmware you need is stored.

```
$ sudo dfu-programmer atmega16u2 erase.
```

Erase the Arduino firmware. (you will be asked for the admin password)

```
$ sudo dfu-programmer atmega16u2 flash arduino_midi.hex
```

Flash the midi firmware.

```
$ sudo dfu-programmer atmega16u2 reset
```

Reset the Arduino.

No, you have to unplug the Arduino and Plugin again. The Arduino should appear as a MIDI device on your computer.

**Attention.**

The Arduino is now set up to send MIDI signals but you can't upload code anymore. To switch back to the original firmware you have to do the same step as written before (including the hardware reset) but instead of flashing the Midi firmware you flash the original firmware (it is in the same folder) with following command:

```
$ sudo dfu-programmer atmega16u2 flash Arduino-usbserial-uno.hex
```

Now you can change the code an upload it again. You can change the firmware as often as you want.

I would recommend to set the sensors and the Arduino up and test it with a sampler on your computer before switching to the Raspberry Pi to make sure the first part of the set up is working fine.

I used a DAW (Logic X in my case) to see what MIDI signals are coming in and Battery as a sampler to test the sensors and to find out what time of delay I want to add to the code.

### Raspberry Pi as a sampler.

Now you can build your own little sampler on the Raspberry Pi. To do so, go to this [link](http://www.samplerbox.org/makeit) and download the software for the Pi. Restore the SD card of the Pi with this image. This will take about 20 minutes. You can do it with the Disk Utility tool (macOS) but I would recommend Terminal. You will find a plenty of guides on how to do so.

After setting up the Pi, you have to prepare your samples. On the page of samplebox, you can download already prepared sample libraries or you can download the two libraries from this repo.

If you want to set up your own library, you simply have to follow this instruction: http://www.samplerbox.org/article/howtocreateasampleset

**Attention:** **It is important that you name the samples exactly as what you defined in the Arduino code before. For example, if you are sending  ** *MIDI.sendNoteOn(36, 100, 1)* **then the file you want to play with this signal should be called something like this  ** *drop_36_vel100.wav* **so the sampler knows this is the sample for the note 36 with the velocity of 100.**

Drag the prepared folder on a USB stick and plug it into the Pi. It should work perfectly. samplerbox is using the onboard soundcard/headphone jack as an output by default.

As it turned out, the Pi was not loud enough. So we had to adjust the default output volume.
To do so, we hooked up the Pi to a TV and a keyboard and booted. The software asks for user and pw. Both is "*root*".

After entering, you will see a comment right above the login request that describes how to switch from read-only to read/write mode, which is essential if you want to save your settings to default. After executing this command, you have to type:

```
alsamixer
```

Now you entering the settings. With tab, you can jump between the settings. With the arrow up and down you can adjust the volume. When finished press EXIT.    

To save this setting as the default setting (otherwise, the Pi will restore the old setting everytime you reboot) you have to type.

```
alsactl store
```

No, it should be fine.

### final setup.

Now you can connect the Arduino to the Raspberry Pi (via USB), plug in the USB stick with the samples to the Pi, switch the Pi on and listen to the signals your sensors producing.

![]({{site.baseurl}}/assets/fabacademy/week17/sketches.jpg)

## 3D Design

[File to download.](https://gitlab.com/MDEF/scribble-sound/tree/master/public/cad%20files)

The design of the structure for scribble sound combined a number of techniques consisting of laser cutting, 3D printing and some improvised building with scrap wood, screws, glue and a nail gun. The structure is large enough for 4 different tracks to be played at once, and its overall dimensions are approximately 300mm x 400mm. These dimensions can be altered and changed at will. Metal rods were used for the tracks to rotate around of 300mm which determined the width of the structure. The length can be changed, but note the laser cutting file will have to be altered, but this will allow for longer tracks to be used.

The structure is designed to be open, so the sensors and arduino are easily accessible and easy to move around when necessary. A smaller structure sits beside one of the edges in order to hold the motor at the correct height so it is inline with the metal rod for when it is rotating.

### Laser Cutting

The laser cutter used to cut the primary structure was a Trotec Speed 400 (1000mm x 600mm). 4mm balsa wood was used as the material and was cut with the following settings:

* Power: 65
* Speed:1
* Hz: 1000

These settings worked for the majority of the structure but due to the balsa wood not laying flat on the bed of the laser cutter, some elements were not cut properly and a craft knife was used to cut through the wood. The cutting less than 5 minutes and was carried out in two passes, holes and smaller elements followed by the outline. The holes were made with a tolerance of 0.15mm which worked well, but could be increased to 0.20mm as some additional sanding was necessary in parts.

### 3D Printing

The spindles were 3D printed. These were made to provide the tracks a larger area to rotate around and prevent them from moving around.

Printers used: Hacked MakerBot for black spindles (x6) and Creality cr-10 (500mm x 500mm bed) for white spindles (x2).
Files were prepared in Rhino and exported as STL.
STL files were then opened and prepared for printing in Ultimaker Cura.

Dimensions of spindles:

* Diameter: 58mm
* Width of dividing edge: 2.5mm
* Width of spindle: 55mm
* Central hole: 8.6mm

The metal rod used had a diameter of 8mm, and an offset of 0.3mm for the central hole is a perfect fit for the rod. This size is large enough so that it is possible to move down the rod, but small enough so there is enough friction between the rod and the inner circle of the spindle for it to rotate correctly and not spin freely when the motor is turned on. Time taken for the printing was very long ~ 18 hours for 8 spindles. To shorten this time the design could be altered so the overall diameter is smaller and the quality of the print could be reduced. The spindles were designed to be solid to provide strength, but this could also be altered so that less material is used and print time is reduced. It is also possible to stack laser-cut rings together if 3D printing is not an option and time is of the essence.

### Additional Structure

There were additional part of wood cut and added to the laser cut balsa wood to add stability as in initial tests with the motor the structure moved around too much. These wooden scraps were cut with standard circular saws and added with screws and a nail gun. More lateral bracing could be designed into the structure which means it could all be laser cut and have a consistent aesthetic.

## Assembling

![]({{site.baseurl}}/assets/fabacademy/week17/IMG_3898.jpg)

![]({{site.baseurl}}/assets/fabacademy/week17/box4.jpg)

## result

<iframe width="560" height="315" src="https://www.youtube.com/embed/BS5BNDIuOpk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
