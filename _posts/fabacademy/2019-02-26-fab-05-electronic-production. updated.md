---
layout: post
title:  "fab 05: electronic production."
date:   2019-02-26 10:00:00 +0530
categories: fabacademy


---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/9204EAAD-F6EC-4673-B862-102DF1CB0ADE-10467-000008F55814D0CA.jpg)

# Electronic production.

------

For this week of the Fab Academy, the challenge was to create your own ISP programmer by milling, soldering and programming. 

I used [this](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week4_electronic_production/fabisp.html) guideline from last years Fab Academy.

## milling.

I started the process with milling. I decided to use the design of [Ali](http://fab.cba.mit.edu/classes/863.16/doc/tutorials/FabISP/FabISP_Demystified.html) I found on the Fab Academy page. It uses the ATiny44 microprocessor.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/hello.ISP.44.traces.png)

I prepared the files with [Fab Modules](http://fabmodules.org) for the Roland SMR 20 milling machine. First I started with the traces with a 1/64 endmill. That worked out pretty well. 

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4641.jpg)

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4640.jpg)

With the outline, I had a bigger issue. The machine was supposed to mil three times in a row on the same path. After the first lap, it moved up on the Z-axis and finished the final 2 laps in the air. I recalibrated the machine but it didn't work. So I set the Z-axis a bit lower and cut only once again. It worked out pretty well in the end.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/57192565395__E24D5CC2-B0C1-47F5-807E-3B58AA86F3E6.jpg)

And this is the result.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4670 (1).jpg)

## soldering.

I wasn't in the soldering class but I got support from my fellow students. I also watched [this](https://www.youtube.com/watch?v=b9FC9fAlfQE) tutorial. 

I collected the following parts first:

- 1 ATTiny 44 microcontroller
- 1 Capacitor 1uF
- 2 Capacitor 10 pF
- 2 Resistor 100 ohm
- 1 Resistor 499 ohm
- 1 Resistor 1K ohm
- 1 Resistor 10K
- one 6 pin header
- 1 USB connector (was not available)
- 2 jumpers - 0-ohm resistors
- 1 Crystal 20MHz
- 2 Zener Diode 3.3 V

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4671.jpg)

To keep them in the order I would recommend to tape them, preferably with transparent tape to the list of components. The component list was not complete as the lab was out of USB connectors. But I did the job without it. That lead to the circumstances that I had no chance to program the board. 

However, I used this plan to see where to solder the components together with the instruction from [this](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week4_electronic_production/fabisp.html) side.

Before I started with the board, I had some laps on a practice board where I unmounted and mounted certain cables to get used to the soldering process. It was my first time.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/hello.ISP.44.png)

In the instructions, it was recommended to start with the USB connector, as it is the most complicated and failing this part could lead to breaking the board. That's, of course, something you want more likely to do in the beginning rather than at the end of the process. 

But, as I had no USB component, I started with the ATiny44. That circumstance didn't save me from failure. cause the internet wasn't not going properly that day, I was trying to remember which way around the microcontroller had to be solder.

Well, I was wrong and after I soldered the controller, I realised and had to remove it. I used the heat gun which made it really easy to take it off again. 

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4672.jpg)

After that unnecessary extra round, I was going on really carefully. But it turned out, I was pretty much the only component where I had to take care of. The following is the order I soldered the components. I added the polarity and the code on the black sheet I posted before. I would recommend keeping this sheet open on your computer, phone or tablet.

1. **Microcontroller**
   - you'll see a little circle on one corner of the controller - that has to point to the little 9 part square shape on the left bottom
   - marked as **"IC1 t44"** on the black sheet
2. **Crystel**
   - has no polarity  (doesn't matter which way around)
   - marked as **"20Mhz"** on the black sheet
3. **Diodes**
   - the polarity of the diodes is marked with a tiny line on the diode. this line has to be on the C pad on the black sheet
   - marked as **"D1" & "D2"** on the black sheet
4. **Resistor** 
   - no polarity
   - marked as
     - R1-----1 K ohm -----1001
     - R2-----499 ohm -----4990
     - R3-----100 ohm -----1000
     - R4-----100 ohm -----1000
     - R5-----10k ohm -----1002
5. **Capacitors**
   - no polarity
   - marked as
     - C1-----1 uf 
     - C2-----10 pf 
     - C3-----10 pf 
6. **Jumpers**
   - no polarity, as they are only jumpers
   - instead of using a 0 Ohm resistor, you could also connect these to parts with solder only
   - marked as  **"SJ1" & "SJ2"** on the black sheet
7. **6 pin programming header**
   - orientation does not matter
   - marked as **"J1 ISP"** on the black sheet

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4676.jpg)

So I have an uncompleted board because of the missing connector. I will continue as soon as we have USB components in stock again.

---

## update:

After the mini USB heads arrived, I soldered it instantly to my board.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4697.jpg)

That worked pretty well. I had to clean a bit the soldering because it seemed to connect to legs with each other what we don't want.

Next step was to program it. I prepared the files on my computer as written in the guide, connected to the programmer, but it didn't work out. 

 ![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4698.jpg)

Of course, the first thing that popped in my mind was that I did something wrong with the soldering. I checked all the connections with the multimeter by checking the black drawing and see if the circuit is closed. And it is… so I will have another run with assistance to find out what I did wrong and will come back with that.

---

## update II:

So I tried to program the board ones again. I followed [this](http://archive.fabacademy.org/archives/2016/doc/programming_FabISP.html#mac) guide step by step.

Here the short version:

1. Download the firmware zip file and place the zip file on your desktop. Here is the [firmware](http://www.as220.org/fabacademy/downloads/fabISP_mac.0.8.2_firmware.zip) for newer macOS systems. 

2. Open terminal or Hyper or whatever you are using and type the following command: 

   ```
   cd ~/Desktop/
   ```

   ```
   unzip fabISP_mac.0.8.2_firmware.zip
   ```

   ```
   cd ~/Desktop/fabISP_mac.0.8.2_firmware
   ```

3. Now, connect the programmer with your board and connect the programmer to your computer. If the LED of the programmer lights up green, the circuit of your board seems to be fine. Does not guarantee that it will work but the first step is done.

4. Now, when the circuit is alright, also connect your board to your computer.

5. Go back to the terminal and type in following commands:

   ```
   cd Desktop/fabISP_mac.0.8.2_firmware
   ```

   ```
   make clean  
   ```

   ```
   make hex
   ```

   ```
   make fuse
   ```

   ```
   make program
   ```

    ![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/Screen Shot 2019-02-28 at 11.58.08.png)

   

   You can check if each step went well by checking the guide. After the last step the last three lines should be something like that:

   ```
   avrdude: verifying ...
   avrdude: 1 bytes of lfuse verified
   
   avrdude: safemode: Fuses OK
   
   avrdude done.  Thank you.
   ```

6. If everything went right, great. Lets test is the board is working. Unplug everything and replug only your board again. Now open up the system profiler of your mac (click on the Apple logo in the upper left corner, select "About This Mac…" and then click "System Report…"), click on USB in the left column and see of "FabISP" is listed in the upper part of the window. If so, you are set and done and can go further to the last step.

After I programmed my board and everything seemed alright I could get rid of the two 0Ohm resistors (jumper) from the board.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4807.jpg)

This makes your board unprogrammable. Means that the firmware you put on will stay on the board. That's a good thing. 

The easiest way to do so was to hold the board with the tweezer on these components and heat up the solder with the heat gun. As soon as the solder is liquified the board just drops and the component will stay in the tweezer.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4809.jpg)

Finally, I removed both jumpers and plugged in the board again to my computer to check if it's still recognised. It seems to work. So I hope I can use this board in the future to program my future boards.

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week5/IMG_4810.jpg)