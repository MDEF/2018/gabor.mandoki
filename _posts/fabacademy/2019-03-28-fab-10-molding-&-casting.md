---
layout: post
title:  "updated: fab 10: casting & molding."
date:   2019-03-28 10:00:00 +0530
categories: fabacademy

---

![by Gabor Mandoki]({{site.baseurl}}/assets/fabacademy/week10/screenshots/Screen Shot 2019-04-22 at 18.25.34.png)

[Download Fusion360 file.]({{site.baseurl}}/assets/fabacademy/week10/files/Heart Mold v5.f3d)

# Molding & Casting.

------

&nbsp;

## Design a 3D object.

It was up to us if we 3D print a mold or mill it with the Roland milling machine with wax. I decided to use the 3D printer as I thought its less messy. I downloaded a nice 3D shape of a human heart and started to create a printable mold. I failed on some point because the downloaded heart hat too many shapes and I wasn't able to convert it into a component.

So I ended up in creating around the heart shape I imported as STL to Fusion. To see how to create 3D objects for 3D printing, follow my documentation from 3D printing week.

I came with this assignment to the stage of planning and creating virtually but didn't#t start to print yet.
