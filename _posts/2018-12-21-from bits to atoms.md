---
layout: post
title:  "From Bits <> Atoms."
date:   2018-12-21 15:00:00 +0530
categories: [term1]

---
![]({{site.baseurl}}/assets/week11/florian-klauer-253-unsplash.jpg)

# Bits <> Atoms.
------

&nbsp;

The week Bits <> Atoms gave us an idea what the FabAcademy could look like. We used the skills we learned over the other two hands-on weeks this trimester.

&nbsp;

### Sound Scribbler.

Together with Julia, Saira, Silvia, Oliver, Ryota and me, we formed a team to create a music machine. We wanted to create a box that makes different sounds by drawing black lines on a white surface.&nbsp;

### ideas.

The first idea we had, was to create a machine referring to a vinyl player. You could draw on your own "vinyl" and put it on our machine to let it play. In the end, we decided to create a less common form factor:

![]({{site.baseurl}}/assets/week11/sketches.jpg)

These first scribbles are showing a box with 5 tracks you can draw on and a sensor from the bottom. We stuck to this first sketch pretty much for our final product.

### prototype.

For the whole project, I mainly focused on programming, sensors and the sound generation.
The first step for me was to figure out how to convert the sensor signals into a reliable musical signal.

*(for the step-by-step guide click [here](https://gitlab.com/MDEF/scribble-sound))*

It wasn't that easy as the sensors weren't precise at all. That resulted in a really randomly sounding machine.

![]({{site.baseurl}}/assets/week11/sensorSetUp_bb.png)

### MIDI to a sampler.

I tested all the sensors with a sampler on my computer and different sounds. In the end, we build a sampler with the Raspberry Pi and created our own chord samples you can also download from [GitLab](https://gitlab.com/MDEF/scribble-sound/tree/master/public/samplesets).

### assembly.

While I figured out the right code and configuration, the others worked on the housing, the drive, power unit and the material for the tracks. First, we tried to use the inner tube of a bike but realised that the material and the colour is unsuitable.

I built the housing for the sensors out of cardboard to fit them as near as possible to the moving surface.

![]({{site.baseurl}}/assets/week11/box4.jpg)



By assembling all items, we realised that we have to redesign some parts as the construction was not strong enough. We had one day left so we had enough time to do so...

![]({{site.baseurl}}/assets/week11/box1.jpg)... and the final box worked pretty well!

<iframe width="560" height="315" src="https://www.youtube.com/embed/BS5BNDIuOpk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### conclusion.

The machine we built is a pretty good prototype we could keep working on. I would replace the sensor with something faster, precise and more reliable. I would also add a speed regulation similar we know from the 808 drum machine for 8 or 16 bars. And I would like to map the different steps on the tracks for being able to generate nice rhythmic patterns. What I missed this week, was to challenge my self to work in fields I am not so comfortable with. The coding and sensor stuff felt very convenient. The next time I will put myself in charge of 3D design or something similar I am not good at to force myself to learn.

### Notes.

[Center of Bits & Atoms.](https://en.wikipedia.org/wiki/Center_for_Bits_and_Atoms)

Document everything - what is not documented does not exist. Including files, progress, fails.

[FABacademy archive.](http://archive.fabacademy.org)
