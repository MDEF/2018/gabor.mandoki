---
layout: post
title: "Emerging Business Models."
date:   2019-06-13 10:00:00 +0530
categories: term3
---

![]({{site.baseurl}}/assets/emerginbusinessmodels/timj-311630-unsplash.jpg)

# Emerging Business Models

## session 1

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQqOulj3rgPtXnBsWtIYKbAPjouDrp-seMx4Ya15zJ19u3UrpkP-ntciLNChBc5Cg5xXxQiRQtHsVzo/embed?start=false&loop=false&delayms=3000" frameborder="0" width="740" height="400" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## session 2

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ-fBIP0-8oDELu-QXa-c2uzgGzsjHBihW4s_8bbFlilgregYJxJZCbxGCMzG-U3c6SDRMzJ6Ud_KJk/embed?start=false&loop=false&delayms=3000" frameborder="0" width="740" height="1200" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## reflection.

The class of emerging business models was exciting in terms of putting our projects into another aspect of the real world context. We, of course, did our research about projects that led to our intervention beforehand, but this time, it was more about how to utilise this already taken efforts to reduce them for ourselves.

The sustainability model of Phone Farm(ing) is built on four pillars: the blockchain based token Attention Coin as currency for every transaction, the farmers who are contributing to the network, the hosts who are hosting the farms and the runners who are deploying their projects on the network.

## Attention Coins

Attention Coin (AC) is a blockchain based token especially emitted for Phone Farm(ing). It will be built on the Ethereum network, a decentralised, open source blockchain platform, that allows you, besides other features, to create your token (Ethereum, 2019).

The number of tokens a farmer earns represents the time spent entirely focused on social interaction. This quantitative measurement is directly convertible into a qualitative measure as we rate time away from the phones in social spaces as a desirable circumstance. Tristan Harris, a founder of the Center of Humane Technology, presented a method of how the quality of service could be measured. With the example of Couchsurfing, he described a model where every design goal had to be linked to the value generated through the rating the user assigned to an experience the service provided - in this case sleeping at a strangers place, combined with the time spent on the online platform what is considered as “cost of peoples life”(Harris, 2014). The resulting score represents the success and quality of the service rather than the quantity.

The Attention Coin embodies a similar way of valuation. The rating of the token depends on the supply and demand of the resources from the Phone Farm(ing) network. If the rating is high, more people are distributing to them, and this equals to the time detached from the personal device. This also means at the same time that there are more resources available on the network, and more space can be purchased with fewer tokens.  

## Farmers

By placing their phones in the farms, farmers are distributing power and data to the network. In return, they receive Attention Coins, depending on the time their devices are connected to the farms. These tokens can be redeemed for rewards the hosts provide like discounts on concert tickets or drinks. Additionally, farmers can assign Attention Coins to projects they would like to support. In this way, the community decides which projects are running on the network as Attention Coins are needed to purchase computing power or data from Phone Farm(ing).

## Runners

To take advantage of the Phone Farm(ing) resources, runners deploy projects for the network. Everyone can be runner, schools or universities, a one-person developer, artists, city councils or labs. Runners can purchase space and data from the network with Attention Coins. They can earn the tokens trough distributing to the system in the first place or buy them from hosts. A third way is to be funded by the community. Farmers and hosts can assign their coins to projects they would like to support.

## Hosts

Hosts are facilities which are providing Phone Farm(ing). The farms are placed mainly in social spaces like restaurants, bars, cinemas and clubs. Educational spaces like schools and universities can host them as well. Especially for the commercially driven locations in the gastronomy and entertainment field, the interest will be in being part of the network of exclusive places providing Phone Farm(ing) as a service. The goal is to set incentives for the hosts to pay for renting the farms.

Additionally, the hosts will be asked to offer some discounts in exchange for Attention Coins or to provide other rewards as exclusive media content, for example. This will also lead to a reduction in the price of renting. To achieve this goal, it is necessary to create a high demand for Phone Farm(ing) in such places. A certificate will be invented that marks hosts as such – similar we know from TripAdvisor or HappyCow. Sites can not only be looked upon the Phone Farm(ing) app but also on TripAdvisor or Google maps by filtering if they only host providing the farms or are also places to redeem Attention Coins. By using service as TripAdvisor, the effort is reduced to reach out to a high number of people as it is already a well-established community. In this way, the adoption by the mainstream will be accelerated.

## Slack-economy

The Phone Farm(ing) network is based on the idea of slack in the ecosystem. Slack is defined as “(…) a measure of the number of unemployed resources.” (Thoma, 2014). These unemployed resources in our system are the idle processing power and the generated sensor data of the smartphones. The potential is huge. If we take the three hours of daily average smartphone usage (Turner, 2018) as a measurement, it means that about 80% of the time smartphones are not in use and so slack in the ecosystem. We already have this discussion with cars, which are standing around the main time of their life span and occupy valuable space in our cities. But it is easier to distribute the vehicle to the sharing economy than a smartphone as a car is less personal. Because of this fact, Phone Farm(ing) is not promoting sharing the device itself to a broader range of users, but its capacities while still maintain the conception of a personal device.   

## Licensing: Creative Commons

Phone Farm(ing) will be distributed under the Creative Commons licence Attribution-NonCommercial-NoDerivs (Creative Commons, n.d.). The project will be available for copying and sharing as longs as it is following the license terms which include appropriate crediting, non-commercial use and no own derivates.

The reason for this licensing is that every project, like DIY farms, for example, should be built with the minimum requirement of contributing to the existing network.   

In this way, the blueprint and programs can be used to build own nodes of the network, but it is not allowing to make the own network based on Phone Farm(ing). Later in the process, this possibility might be released, but the success of this system also relies on the number of nodes.  

## Open-source elements used for Phone Farm(ing)

For the implementation of Phone Farm(ing), different already mentioned open source components will be used.

### Golem Network

The Golem Network is a project for an open-source computing power platform. Right now, it is in a beta phase and only focusing on the home computing sector. According to Wiktor Nowakowski from Golem, the protocol this network is based on, is platform agnostic, meaning it is portable to smartphone operating systems.   

### Ethereum Network

Ethereum is an opensource blockchain network that allows developing decentralized apps (DAPP) on its platform. It will be used to provide a decentralized infrastructure for Phone Farm(ing), to issue the Attention Coins as well as to generate smart contracts for transactions.

Blockchain is used because of multiple aspects. First of all, the privacy of farmers has to be protected as they provide sensitive data from their device. All data collected has to be anonymized and untraceable to the real identity of the contributor. Second, the whole system is decentralized as the community owns it. The community provides the sources, selects and hosts the projects without having one central power to control any of these aspects. An additional factor to this decentralization is transparency. Every transaction, project and process is available. The third aspect is the Attention Coin token, which takes advantage of the blockchain based cryptocurrency technology available.
