---
layout: post
title:  "navigating the uncertainty."
date:   2018-11-02 19:45:31 +0530
categories: [term1]
---

![by Gabor Mandoki]({{site.baseurl}}/assets/week5/MG_9452.jpg)

# navigating the uncertainty.
---
&nbsp;

### day 1.

*Presentation by Jose Luis de Vicente.*

[IAAC lecture series](https://www.youtube.com/watch?v=DWosq6BBzTk)

### How do we think about our relationship to the planet?

There are obviously different scales for every one of us.
Some doesn't even think about living on a planet as such. Describing a relationship to something so obvious isn't easy but makes you think. Is it the soil underneath your toes or the air you breathe?

Then the second point he brought up was, ***how we measure the sense of now?***
He showed some radical events of the last years, decades and centuries when we could set the starting point of now:

now can be **one month** *(Catalunya)*  

now can be **one year** *(Brexit, Trump)*

now can be **10 years** *(finacial crises)*

now can be **9/11**

now can be **150 years** *(industrial revolution, burning fossil fuels)*

now can be **12.000 years** *(starting of agriculture)*

*but scientifically the most precise definition is*

**Juli, 16th 1945 - the first detonation of a nuclear bomb in Mexico.**

the **great acceleration** or the day the mad-made-activities of mankind became superior over nature.
From this moment on mankind took over to shifting the planet in a very radical way.

If we set this as our starting point for now, then we should summarize what happened since then and what we are heading to.

***"The good news is: this civilization is over. And everybody knows it. And the good news is: we can all start building another one, here in the ruins, and out of pieces of the old one."*** *by McKenzie Wark*

Luis presented different concepts of dealing with the climate crises.

[article about Accelerationism & degrowth.](https://www.degrowth.info/en/2017/08/accelerationism-and-degrowth/)

For me, two of the 5 ideas are the most realistic. Acceleration & Mutualism.
Extinction for me is only the consequence if nothing else works out...

The reason is simple. In history, people always tried to keep their habits as long as possible and put a lot more effort into keeping them instead of improving and moving on. That's why we are now at the peak of defending our comfort zones instead of changing even though knowing better. Otherwise, we would quit producing plastic, coal mining, nuclear, business with weapons, exploiting humans and nature and the list goes on and on and on and on...

&nbsp;

### Conclusion:

I have to say, trying to find an explicit date for when the humanity started to let's call it to shape rather than destroy the planet, shows how much effort we put to create our bubble of existence and how much impact our behaviour has for thousands and thousands of years. We are struggling more to change our self than to move mountains from one side of the earth to the other. If we would put as much effort to keep our habits as to change our (bad)habits to preserve what is left, probably we wouldn't lose the battle since the 1980s when the first public outcry happened.

In addition, Mariana asked why we want to go reverse instead of going on. I agree on that motivation but would like to add that I don't think that we go reverse by trying to radically change our habits to keep what's left.

[video: TED talk](https://www.ted.com/talks/per_espen_stoknes_how_to_transform_apocalypse_fatigue_into_action_on_global_warming?language=en)
[video: 7 cheap things](https://www.youtube.com/watch?v=BAJgGFtF44A)
[book: How to thrive in the next Economy.](http://thackara.com/thackarathrive/)

&nbsp;

### day 2.

*Presentation by Pau Alsina.*

&nbsp;

***"Design is an effect rather than a process."***

&nbsp;

#### Speed Bump and the forcing chair.

Does design create society? Does design shape society?

Well, depends on the definition of society. But I would say yes of course. Design can influence behaviour, desires, communication, and many more things which are core elements of social living and society.

I liked the simplicity of the ideas Pau told us about. The chair example where people are forced to sit straight or the street bumper where the people are pushed to behave right for social convention motivated by personal concern. Of course, these two principles can be used as construction plans for your own design and if you want to influence or build social relations. It also made me think if these principles are applicable for our next chapter of design for the real digital world when the human centralized design will shift into a multi-interfaced construct as artificial participants will be part of society.

[Discipline and Punish](https://www.goodreads.com/book/show/80369.Discipline_and_Punish)//
[book: Cosmopolitical Design](https://www.amazon.es/Cosmopolitical-Design-Nature-Built-Environment/dp/1472452259)//
[NewYorker articel](https://www.newyorker.com/news/our-columnists/the-fifteen-year-old-climate-activist-who-is-demanding-a-new-kind-of-politics)//
[Do Artifacts have politics (1980)](https://www.cc.gatech.edu/~beki/cs4001/Winner.pdf)

&nbsp;

#### Technology vs Nature.

As we had a discussion about what is technology and what is nature and where to draw the line, I tried to define it for myself.
In contrast to the discussion, I wouldn't call a human body for example technology. Otherwise, everything would be technology and we wouldn't need to differentiate.
For me, technology and nature are strictly dividable. Nature is created by biomechanics like evolution and technology is artificially created for solving problems on purpose by conches creatures or technology itself.
We have crossings of course where technology is added to nature. But still, by adding technological components, natural objects become technology. We invented a terminology for adding chips to our bodies, "Cyborg", which implements that we turn into a technological object.

&nbsp;

### day 3.

*[presentation](https://docs.google.com/presentation/d/1tH-p7dmya0MSWEPhSCQ1E_yI9748MBSNCOes92W4Lbc/edit#slide=id.p) by Mariana*

&nbsp;

***How to transform the philosophy of the day before into the digital environment?***

&nbsp;

*read-write technologies. basses of information exchange. all physical systems are based on an exchange of information.*

Limits are membranes where information is exchanged. The question is how can we transform these principals of biology into a non-biological environment? What we did so far was to create membranes for interaction. The most popular example is the screen of our phones we use every day. But how do we have to change from human centralized interaction design to a way where we include all our artificially created participants in these environments? What could an interface look like for cars communicating with each other without any human interaction in between?

Of course, we have systems for machines communicating with each other, build on the principles of communication of humans and binary. But I am wondering what will happen when the machines start to learn end evolve themselves and optimize their way of communication and interaction so that humans are not even capable of understanding it. There is a popular example of this: [Facebook AI created his own language to make communication more efficient.](http://www.digitaljournal.com/tech-and-science/technology/a-step-closer-to-skynet-ai-invents-a-language-humans-can-t-read/article/498142)

***How much would the core ideas of our cultural habits change when we change our interface?***

How would our cultural habits or core values change if we would have other capabilities in sensing like haven the capability in seeing x-ray? I think this is a good thought model how machines see the world probably. Different senses, different interfaces, different environment.

*I have to read some more papers here to have a final conclusion but will add this over the time.*

[book: Life 3.0](https://en.wikipedia.org/wiki/Life_3.0)
[Jeremy England: The next Darwin](https://en.wikipedia.org/wiki/Jeremy_England)
[Saskia Sassen](https://en.wikipedia.org/wiki/Saskia_Sassen)
[Neil Harbission](https://de.wikipedia.org/wiki/Neil_Harbisson)
[Technics & Civilization](https://en.wikipedia.org/wiki/Technics_and_Civilization)
[The three body porblem](https://www.goodreads.com/book/show/20518872-the-three-body-problem)
[Cybogars](https://www.cyborgarts.com)
[Dear Data.](https://www.dear-data.com/theproject)

&nbsp;

### The evolved vision after this week...

I strongly believe in a future scenario driven by transparency.
After we missed the chance in the times when radio, tv or the internet were new technologies to decentralize the suppliers of information we now have a new chance to do so. Blockchain for me is a technology, if we use it right, it could lead to a transparent, decentralized world with spread power to every user.

A lot of trouble is caused by the hidden figures and deals behind the curtains in politics and economy.
People are losing trust in the so-called "establishment" and shifting to extreme political sides.

I believe that there is no need to keep things secret - but only if everything is transparent.

[Open Goverment](https://en.wikipedia.org/wiki/Open_government)
[Wikileaks](https://en.wikipedia.org/wiki/WikiLeaks)
[Sharing Economy & Blockchain.](https://hbr.org/2017/03/what-blockchain-means-for-the-sharing-economy)
[The Myth of Freedom](https://www.theguardian.com/books/2018/sep/14/yuval-noah-harari-the-new-threat-to-liberal-democracy)

As much as I believe in transparency, I also believe in privacy. For being transparent in production chains, politics or finances no-one has to give up privacy. We empowered companies by giving them our data literally for free. We should promote to get this power back to us and be aware of deciding what we want to give and what not.

*alternate services focusing on privacy:*

Google alternative: [DuckDuckGo.](https://duckduckgo.com)

Whats App alternatives: [Signal.](https://signal.org) [Threema.](https://threema.ch/)

Chorme & Safari alternative: [Brave.](https://brave.com)

Dropbox & GoogleDrive alternative: [mega.](https://mega.nz)

VPN client: [Disconect Pro.](https://disconnect.me)

Artificial Intelligence for me could be a chance to push the idea of a more diverse and fair society. We could create different languages for different purposes which are not bias. Languages without gender or any cultural vocabulary to program these machines. This could help to minimize the natural human prejudice. It also could help us to reevaluate our moral and ethical codexes with a global approach.

[Diversity in AI](https://medium.com/@racheltho/diversity-crisis-in-ai-2017-edition-ce20f11f1230?email=gabormandoki%40me.com&g-recaptcha-response)

For all this, in the end, education is key. Education has to be reformed radically. Information is everywhere, but we have to learn how to get access and how to classify the information. We could focus more on how to learn rather than what to learn.

I have to work on this perspective and gather a lot more background information. Then I will create a portfolio of tools promoting this idea of transparency, privacy, and diversity.

[book: Jordan Petersen - 12 Rules of Life.](https://en.wikipedia.org/wiki/12_Rules_for_Life)

[citizen participation.](http://consulproject.org/en/)
[El Sistema.](https://en.wikipedia.org/wiki/El_Sistema)
[Turotials.](https://www.plethora-project.com/education/)

&nbsp;

### **positiv design.**

[sent by Emily.](http://culturesofresilience.org/wp-content/uploads/resources/CoR-Booklet-forHomePrinting.pdf) //
[sent by Mariana.](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/) //
[sent by Fifa.](https://www.ted.com/talks/per_espen_stoknes_how_to_transform_apocalypse_fatigue_into_action_on_global_warming/up-next?language=en)
