---
layout: post
title:  "Design for the New: Practice Map."
date:   2019-05-09 10:00:00 +0530
categories: term3

---
![]({{site.baseurl}}/assets/designforthenew/presentSoicalPractice.png)

# Practice Map.

In this weeks class of "Design for the New" by Markel Cormenzana & Mercè Rua from Holon, we were asked to map out the practices of our intervention to day and in the future in the categories

- images
- skills &
- stuff

*What elements and links constitutes this practice?*

### Images *(Emotions, meanings, cultural narratives & identity)*

- looking on the phone
- dinner
- concert
- connect all around the world
- being notified
- instant gratification
- fear of missing out
- always available

#### connection to skills: Who is not in is out...

### Skills *(Understanding, knowledge, know how & abilities)*

- writing
- having conversation
- taking photos
- being social
- write a message
- like a picture
- acknowledge a good face to face conversation
- aware of manipulation and distraction

#### connection to stuff: The one or the other...

### Stuff *(Materials, technology & artifacts)*

- phone
- drink
- internet
- words
- display
- eyes

#### connection to images: I have to be available - anytime...

---

![]({{site.baseurl}}/assets/designforthenew/futureSocialPractice.png)

## Practice map of phone usage & communication (future)

*What elements and links constitutes these practices?*

### Images *(Emotions, meanings, cultural narratives & identity)*

- no phone
- undistracted conversation
- invisible technology
- intimacy
- focusing on each other
- being fully part
- social interaction
- dinner
- drinks
- enjoyment

#### connection to skills: I am having a full-filling face to face conversation.

### Skills *(Understanding, knowledge, know how & abilities)*

- conscious acting & behavior
- conversation
- listening
- empathy
-

#### connection to stuff: One leads to the other.

### Stuff *(Materials, technology & artifacts)*

- device
- Phone Farms
- locations for social interaction
- network

#### connection to images: Trigger the dialogue.

## conclsuion and application to master project. 

I was inspired by these classes and applied the analysis of social practices to my design actions. 

The more complicated part after raising awareness about having an issue is to influence or even to change habits. The only way to do so is training. The challenge is to minimise 'FOMO’ and to recondition the feeling of being unattached with something positive. 

'A routinised type of behaviour which consists of several elements, interconnected to one other: forms of bodily activities, forms of mental activities, 'things' and their use, background knowledge in the form of understanding, know-how, states of emotion and motivational knowledge.' 
 (Reckwitz, 2002)

First, we have to analyse today's social practices related to the smartphone’s influence on social interaction. 
 As an example, we examine the social practice of going out for a coffee with a friend. 
 Both participants are in the millennial’s generation in the age of 18-23, as they represent a profoundly affected milieu. In this image, both are sitting in a café. The smartphones are lying face down on the table. Conversations are taking place. During the whole time, the devices vibrate in irregular intervals, and one or both are checking what came in. In some situations, the participants disappear in their digital world for a while, and face-to-face interaction has completely stopped. 

These practices lead not only to interruptions in conversations but also reduces the level of enjoyment of such events. In a study from the year 2018, an experiment with 300 people showed that the presence of phones during meal situations undermine the usual benefits we have through social interaction (Dwyer et al., 2018).

The circumstance of having the phone present, or at least semi-present around us most of the times is distractive and impairs face-to-face interaction.

The desired future practice is to create the same social situation as going out for a coffee, but with a very natural routine to put down the phone to a farm like the coat to a coat rack when entering a social space. To achieve this goal, Phone Farm(ing) offers three things. One is the sheer option to lock and charge the phone out of sight in these facilities similar to the charging points you find at airports around the world. The second will be showing how the phone contributes to the community in the meantime. And thirdly, will be a rewarding system to increase motivation.  

## Design actions

### Collective Edibles experience dinner

The first design action was the creation of a trial scenario to analyse people’s reactions and to open up discussions around that topic. 

Together with Emily Whyman and her “Collective Edibles”-project, we hosted an experience dinner at restaurant LEKA end of May. LEKA was the perfect location as it is known for its open source ethos. All recipes and furniture is available online for reproduction. Before the meal started, every guest was asked to put his phone in a black box. The box was then brought in the back of the restaurant so that the devices were entirely out of sight. Before taking them, I announced that I would take pictures and provide them right afterwards for sharing. 

During the dinner, I asked from time to time if someone is missing their phone. Reactions were divided into no and a maybe a bit. Only once or twice situations came up, that one of the guests wanted to show something on the phone to another. But the overall scene was a dinner full of conversations, and the general feedback was positive. 

As soon as I gave back the phones after dinner, every participant immediately disappeared in the digital world and checked how many notifications were received over the time of the feast. This situation was in total contrast to the rest of the night. 

After dinner, we sent out a survey. Two-thirds of the guest said that they were affected by the fact being without the phone during the meal. Also, two-thirds admitted that they had the urge to check social media at least sometimes. One third said that they hadn't had the urge at all. Every participant answered yes to the question if it would make a difference if the phone is contributing to education, science or art projects for that time. The same share answered yes to the question if they could imagine using Phone Farm(ing) in social spaces in the future. The question regarding rewards was individually answered. One third said that they don’t need a reward at all. Others asked for an asset that could be used in a transaction, for example.

We interviewed our guest a few days after the dinner about the experience. Most admitted that it made a difference not having the phone during the dinner. Additionally, the fact that the pictures were sent out after the dinner was pointed out as proper compensation for not being able to take photos during the dinner. You will find the gallery and a Spotify-playlist which was created as an additional reward following this link: [phonefarm.eu/gallery](http://www.phonefarm.eu/gallery)

An edit of the recorded interviews will be released on [phonefarm.eu](http://www.phonefarm.eu/).

The outcome of this event was a very successful first shot with room to improve. Part of the experiment was only to give minimum context to see the reactions about the fact that there are no phones in the game only. But next time, I would provide more information beforehand also to promote a discussion about the framework. I will also create a showcasing voting system for the next event where every participant can vote for a project the network is working towards. 

Furthermore, a second event is in planning. This time, a prototype will be placed, where every guest will be able to lock in the phone by themselves as well as keeping the opportunity to access the smartphone anytime. The voting system will be set up as well, as mentioned before. I expect more engagement with the possible projects and also the desired competition among farmers about who can lock away the phone for a longer time. Moreover, single-use cameras will be issued for taking pictures of the event and to avoid the feeling of missing out of opportunities to catch memories.  

As future design actions, I propose to offer these kinds of events together with Collective Edibles as an Experience on Airbnb to reach out to a broader range of people. [Airbnb Experiences Barcelona](https://www.airbnb.com/s/experiences?refinement_paths[]=%2Fexperiences&query=Barcelona%2C Spain&search_type=SEARCH_QUERY&place_id=ChIJ5TCOcRaYpBIRCmZHTz37sEQ)

### Next steps

The next step is to scale up the experience. A booth on a festival is the perfect framework to catch up with a more significant number of people. By offering free charge, an essential benefit on festivals, you can open up discussions about the topic. Moreover, conferences and events related to blockchain or cloud computing topics offer the possibility to discuss possible projects running on the network. 